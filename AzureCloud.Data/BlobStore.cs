﻿using AzureCloud.Core.BlobHelper;
using AzureCloud.Core.Common;
using AzureCloud.Core.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
    public class BlobStore : IBlobStore
    {
        private readonly CloudStorageAccount _account;
        private string _containerName;
        private readonly CloudBlobContainer _container;
        public BlobStore(CloudStorageAccount account, string containerName)
        {
            _account = account;
            _containerName = containerName;

            var client = _account.CreateCloudBlobClient();
            client.DefaultRequestOptions.RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(3), 10);
            _container = client.GetContainerReference(containerName);
            _container.CreateIfNotExists();

        }

        public async Task<List<BlobUploadModel>> UploadBlob(HttpContent httpContent, string subDirectory)
        {
            var blobUploadProvider = new BlobStorageUploadProvider();

            blobUploadProvider.BlobContainer = _container;
            blobUploadProvider.SubDirectory = subDirectory;

            var list = await httpContent.ReadAsMultipartAsync(blobUploadProvider).ContinueWith(task =>
            {
                if (task.IsFaulted || task.IsCanceled)
                {
                    if (task.Exception != null) throw task.Exception;
                }
                var provider = task.Result;
                return provider.Uploads.ToList();
            });

            return list;
        }

        public async Task<BlobDownloadModel> DownloadBlob(string directory, string blobId)
        {
            var blobName = directory + "/" + blobId;
            var blob = _container.GetBlockBlobReference(blobName);
            if (blob.Exists())
            {
                var ms = new MemoryStream();
                await blob.DownloadToStreamAsync(ms);

                // Strip off any folder structure so the file name is just the file name
                var lastPos = blob.Name.LastIndexOf('/');
                var fileName = blob.Name.Substring(lastPos + 1, blob.Name.Length - lastPos - 1);
                // Build and return the download model with the blob stream and its relevant info
                var download = new BlobDownloadModel
                {
                    BlobStream = ms,
                    BlobFileName = fileName,
                    BlobLength = blob.Properties.Length,
                    BlobContentType = blob.Properties.ContentType
                };

                return download;
            }

            return null;

        }
        public async Task<UploadViewModel> Upload(FileViewModel fileViewModel)
        {
            UploadViewModel _uploadedFile = new UploadViewModel();
            try
            {
                if (fileViewModel.FileStream == null || fileViewModel.FileStream.Length <= 0 || fileViewModel.FileStream.Length <= 17)
                {
                    _uploadedFile.Message = "File to upload is not included or data not sent.";
                    return _uploadedFile;
                }

                if (string.IsNullOrEmpty(fileViewModel.Name))
                {
                    _uploadedFile.Message = "File to upload Name is not provided. ";
                    return _uploadedFile;
                }

                string fileType = Path.GetExtension(fileViewModel.Name);
                if (fileType == null || fileType.Length == 0)
                {
                    _uploadedFile.Message = "File extension missing";
                    return _uploadedFile;
                }

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = AzureHelper.GetMasterDataCloudStorageAccount();

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //blobClient.RetryPolicy = Microsoft.WindowsAzure.Storage.RetryPolicies.RetryContext

                // Retrieve reference to a previously created container.
                // Basing on the purpose of the file, the container will have to be created.
                //string containerName = ConfigurationManager.AppSettings.Get("Storage:" + file.Purpose.ToString());
                CloudBlobContainer container = blobClient.GetContainerReference(fileViewModel.BlobContainerName.ToLower());

                await container.CreateIfNotExistsAsync();

                // Sometimes the filename has a leading and trailing double-quote character
                // when uploaded, so we trim it; otherwise, we get an illegal character exception
                //var fileName = Path.GetFileName(FileData.Headers.ContentDisposition.FileName.Trim('"'));
                // Retrieve reference to a blob named fileName.  
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileViewModel.SubDirectory + "/" + fileViewModel.Name);

                // Set the blob content type
                blockBlob.Properties.ContentType = "MediaType";

                // Add Metadata if provided along with the file,
                if (fileViewModel.MetaDataList != null)
                {
                    foreach (FileMetaData meta in fileViewModel.MetaDataList)
                    {
                        blockBlob.Metadata.Add(meta.Name, meta.Value);
                    }
                }

                // Create or overwrite the "myblob" blob with contents from stream
                // In case of Web File Stream is not supported through System.IO.
                // So created another property of byte array and uploading from byte array.
                if (fileViewModel != null && fileViewModel.FileContent != null && fileViewModel.FileContent.Length > 0)
                {
                    await blockBlob.UploadFromByteArrayAsync(fileViewModel.FileContent, 0, fileViewModel.FileContent.Length - 1);
                }
                // In case of API Steam is working and we upload from Stream.
                else
                {
                    await blockBlob.UploadFromStreamAsync(fileViewModel.FileStream);
                }
                _uploadedFile.State = true;
                _uploadedFile.Message = "Success";
                _uploadedFile.UploadedURL = blockBlob.Uri.ToString();
            }
            catch (Exception ex)
            {
                _uploadedFile.State = false;
                _uploadedFile.Message = ex.Message.Trim();
            }
            return _uploadedFile;
        }
        public async Task<bool> DeletePatientProfileImage(string directory, string blobId)
        {
            bool status = false;
            try
            {
                CloudStorageAccount storageAccount = AzureHelper.GetCloudStorageAccountObjectBasedOnDeploymentType();

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //blobClient.RetryPolicy = Microsoft.WindowsAzure.Storage.RetryPolicies.RetryContext

                // Retrieve reference to a previously created container.
                // Basing on the purpose of the file, the container will have to be created.
               // string containerName = ConfigurationManager.AppSettings.Get("Storage:" + file.Purpose.ToString());

                // Create the table client. 
                var blobName = directory + "/" + blobId;
                // var blob = _container.GetBlockBlobReference(blobName);
                CloudBlobContainer container = blobClient.GetContainerReference("click2clinic");
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
                status= blockBlob.DeleteIfExists(DeleteSnapshotsOption.DeleteSnapshotsOnly);
               // status = container.DeleteIfExists();
            }
            catch (Exception ex)
            {

            }
            return status;
        }
    }
}
