﻿using AzureCloud.Core;
using Incus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
   public interface IAzureUserRepository
    {
       bool AddRecord(UserRegistrationEntity userreg);
    }
}
