﻿using AzureCloud.Core;
using AzureCloud.Core.Common;
using AzureCloud.Core.Extensions;
using Incus.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
    public class AzureSearchRepository : IAzureSearchRepository
    {
        public AzureSearchRepository()
        {

        }

        public async Task<List<SearchUserInfo>> SearchForUsersBasedOnName(string searchStr, string mobileNumber)
        {
            CloudStorageAccount account = AzureHelper.GetCloudStorageAccountObjectBasedOnDeploymentType();
            // Create the table client. 
            CloudTableClient tableClient = account.CreateCloudTableClient();
            // Create the CloudTable object that represents the "Subjects" table. 
            CloudTable table = tableClient.GetTableReference("UserRegistration");


            if (string.IsNullOrEmpty(searchStr)) return null;

            char lastChar = searchStr[searchStr.Length - 1];
            char nextLastChar = (char)((int)lastChar + 1);
            string nextSearchStr = searchStr.Substring(0, searchStr.Length - 1) + nextLastChar;
            string prefixCondition = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("Name", QueryComparisons.GreaterThanOrEqual, searchStr),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("Name", QueryComparisons.LessThan, nextSearchStr)
                );

            TableQuery<SearchUserInfo> query = new TableQuery<SearchUserInfo>()
                    .Where(
                       TableQuery.CombineFilters(prefixCondition,
                                   TableOperators.And,
                           TableQuery.GenerateFilterCondition(
                                   "RowKey", QueryComparisons.NotEqual, mobileNumber)

                                                              ));

            List<SearchUserInfo> SerachUserInfoList = new List<SearchUserInfo>();
            var results = await table.ExecuteQueryAsync(query);
            SerachUserInfoList = results.Any() ? results.ToList() : new List<SearchUserInfo>();

            return SerachUserInfoList;
        }
        public async Task<List<SearchUserInfo>> FindFriendsFromAure(int startIndex, int noOfRecordsFetchForFindFriends,string mobileNumber)
        {
            CloudStorageAccount account = AzureHelper.GetCloudStorageAccountObjectBasedOnDeploymentType();
            // Create the table client. 
            CloudTableClient tableClient = account.CreateCloudTableClient();
            // Create the CloudTable object that represents the "Subjects" table. 
            CloudTable table = tableClient.GetTableReference("UserRegistration");
            List<SearchUserInfo> SerachUserInfoList = new List<SearchUserInfo>();
            TableQuery<SearchUserInfo> query = new TableQuery<SearchUserInfo>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.NotEqual, mobileNumber));

            var results = await table.ExecuteQueryAsync(startIndex, query, noOfRecordsFetchForFindFriends);
            results = results.Take(noOfRecordsFetchForFindFriends).ToList();
            SerachUserInfoList = results.Any() ? results.ToList() : new List<SearchUserInfo>();

            return SerachUserInfoList;
        }
    }
}
