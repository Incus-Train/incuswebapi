﻿using AzureCloud.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
   public interface IBlobStore
    {
        Task<List<BlobUploadModel>> UploadBlob(HttpContent httpContent, string subDirectory);
        Task<BlobDownloadModel> DownloadBlob(string subDirectory, string blobId);
        Task<UploadViewModel> Upload(FileViewModel fileViewModel);
        Task<bool> DeletePatientProfileImage(string directory, string blobId);
    }
}
