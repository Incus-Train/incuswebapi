﻿using AzureCloud.Core;
using AzureCloud.Core.Common;
using AzureCloud.Core.Extensions;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
    public class LearnMasterDataRepository: ILearnMasterDataRepository
    {
        public LearnMasterDataRepository()
        {
            //AzureTableHelper.GetCloudTable(LEARN_SUBJECTS_TABLE, true);
        }

        public async Task<List<SubjectsEntity>> GetSubjectsListForDiscussionForumFromAzure()
        {
            return await GetSubjectsList();
        }
        public async Task<List<SubjectsEntity>> GetSubjectsListForChatFromAzure()
        {
            return await GetSubjectsList();
        }
        public async Task<List<SubjectsEntity>> GetSubjectsListForVideoFromAzure()
        {
            return await GetSubjectsList();
        }
        private async Task<List<SubjectsEntity>> GetSubjectsList()
        {
            CloudStorageAccount account = AzureHelper.GetMasterDataCloudStorageAccount();
            // Create the table client. 
            CloudTableClient tableClient = account.CreateCloudTableClient();
            // Create the CloudTable object that represents the "Subjects" table. 
            CloudTable table = tableClient.GetTableReference("Subjects");
            TableQuery<SubjectsEntity> query = new TableQuery<SubjectsEntity>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey",
                                                              QueryComparisons.Equal,
                                                              "C2C"));
            List<SubjectsEntity> SubjectsEntityList = new List<SubjectsEntity>();
            var results = await table.ExecuteQueryAsync(query);
            foreach(var item in results)
            {
                item.SubjectIdForSort = Convert.ToInt32(item.SubjectId);
            }
            SubjectsEntityList = results.Any() ? results.ToList().OrderBy(x=>x.SubjectIdForSort).ToList() : new List<SubjectsEntity>();

            return SubjectsEntityList;
        }
        public async Task<List<CourseInfo>> GetCourseInfoForDiscussionForumFromAzure(string subjectCode)
        {
            CloudStorageAccount account = AzureHelper.GetMasterDataCloudStorageAccount();
            // Create the table client. 
            CloudTableClient tableClient = account.CreateCloudTableClient();
            // Create the CloudTable object that represents the "Subjects" table. 
            CloudTable table = tableClient.GetTableReference("Courses");
            TableQuery<CourseInfo> query = new TableQuery<CourseInfo>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey",
                                                              QueryComparisons.Equal,
                                                              subjectCode));
            List<CourseInfo> CourseInfoList = new List<CourseInfo>();
            var results = await table.ExecuteQueryAsync(query);
            CourseInfoList = results.Any() ? results.ToList() : new List<CourseInfo>();

            return CourseInfoList;
        }
        public async Task<List<CourseInfo>> GetCourseInfoForChatFromAzure(string subjectCode)
        {
            CloudStorageAccount account = AzureHelper.GetMasterDataCloudStorageAccount();
            // Create the table client. 
            CloudTableClient tableClient = account.CreateCloudTableClient();
            // Create the CloudTable object that represents the "Subjects" table. 
            CloudTable table = tableClient.GetTableReference("Courses");
            TableQuery<CourseInfo> query = new TableQuery<CourseInfo>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey",
                                                              QueryComparisons.Equal,
                                                              subjectCode));
            List<CourseInfo> CourseInfoList = new List<CourseInfo>();
            var results = await table.ExecuteQueryAsync(query);
            CourseInfoList = results.Any() ? results.ToList() : new List<CourseInfo>();

            return CourseInfoList;
        }
        public async Task<List<CourseInfo>> GetCourseInfoForVideoFromAzure(string subjectCode)
        {
            CloudStorageAccount account = AzureHelper.GetMasterDataCloudStorageAccount();
            // Create the table client. 
            CloudTableClient tableClient = account.CreateCloudTableClient();
            // Create the CloudTable object that represents the "Subjects" table. 
            CloudTable table = tableClient.GetTableReference("Courses");
            TableQuery<CourseInfo> query = new TableQuery<CourseInfo>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey",
                                                              QueryComparisons.Equal,
                                                              subjectCode));
            List<CourseInfo> CourseInfoList = new List<CourseInfo>();
            var results = await table.ExecuteQueryAsync(query);
            CourseInfoList = results.Any() ? results.ToList() : new List<CourseInfo>();

            return CourseInfoList;
        }
    }
}

