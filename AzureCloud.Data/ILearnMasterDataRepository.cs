﻿using AzureCloud.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
   public interface ILearnMasterDataRepository
    {
        Task<List<SubjectsEntity>> GetSubjectsListForDiscussionForumFromAzure();
        Task<List<SubjectsEntity>> GetSubjectsListForChatFromAzure();
        Task<List<SubjectsEntity>> GetSubjectsListForVideoFromAzure();
        Task<List<CourseInfo>> GetCourseInfoForDiscussionForumFromAzure(string subjectCode);
        Task<List<CourseInfo>> GetCourseInfoForChatFromAzure(string subjectCode);
        Task<List<CourseInfo>> GetCourseInfoForVideoFromAzure(string subjectCode);
    }
}
