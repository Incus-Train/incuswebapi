﻿using AzureCloud.Core;
using AzureCloud.Core.Common;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;


namespace AzureCloud.Data
{
    public class AzureUserRepository : IAzureUserRepository
    {
        public AzureUserRepository()
        {

        }

        public bool AddRecord(UserRegistrationEntity userreg)
        {
            try
            {
                CloudStorageAccount account = AzureHelper.GetCloudStorageAccountObjectBasedOnDeploymentType();
                // Create the table client. 
                CloudTableClient tableClient = account.CreateCloudTableClient();
                // Create the CloudTable object that represents the "Subjects" table. 
                CloudTable table = tableClient.GetTableReference("UserRegistration");

                TableOperation insertOperation = TableOperation.InsertOrReplace(userreg);
                var res = table.Execute(insertOperation);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
