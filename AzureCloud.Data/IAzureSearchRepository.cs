﻿using AzureCloud.Core;
using Incus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Data
{
   public interface IAzureSearchRepository
    {
        Task<List<SearchUserInfo>> SearchForUsersBasedOnName(string searchStr, string mobileNumber);
        Task<List<SearchUserInfo>> FindFriendsFromAure(int startIndex,int noOfRecordsFetchForFindFriends,string mobileNumber);
    }
}
