﻿using Incus.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Incus.Models;
using System.Web.Routing;
using AzureCloud.Data;
using Incus.Core.Common;
using System.Web.Http.Cors;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Runtime.Remoting.Messaging;
using System.Net;
using System.Net.Http;
using System.Threading;

namespace IncusWebAPI.Controllers
{
    public class INCUSController : ApiController
    {
        private INCUSRepository _connectedRepo;
        public INCUSController()
        {
            _connectedRepo = new INCUSRepository();
        }
      

        #region Login
        [Route("VerifyUser/{UserID}/{Password}", Name = "VerifyUser")]
        [HttpGet]
        public async Task<LoginResponseModel> VerifyUser(string UserID, string Password)
        {
            LoginResponseModel responseViewModel = new LoginResponseModel();
            string TempPatientID = string.Empty;
            try
            {
                var responseData = await _connectedRepo.INCUS_VerifyOTPWeb(UserID, Password);
                if (responseData != null)
                {
                    responseViewModel = responseData;
                }
                else
                {
                    responseViewModel = responseData;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(Password);
                string ErrorSource = "Controller:INCUSController.cs;Method:INCUS_VerifyOTPWeb;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return responseViewModel;
        }
        [Route("INCUSUserRegistration", Name = "SaveIncusUserRegistration")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> SaveIncusUserRegistration(INCUSRegisterModel SaveUserRegistration)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.SaveIncusUserRegistration(SaveUserRegistration);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(SaveUserRegistration);
                string ErrorSource = "Controller:INCUSController.cs;Method:SaveIncusUserRegistration;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("UpdatePhoneno", Name = "UpdatePhoneno")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> UpdatePhoneno(INCUSRegisterModel SaveUserRegistration)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.UpdatePhoneno(SaveUserRegistration);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                    responseViewModel.Mobileno = responseData.Mobileno;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(SaveUserRegistration);
                string ErrorSource = "Controller:INCUSController.cs;Method:UpdatePhoneno;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("VerifyUser", Name = "Incus_VerifyLogin")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> Incus_VerifyLogin(INCUSVerifyUserModel SaveUserRegistration)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.Incus_VerifyLogin(SaveUserRegistration);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                    responseViewModel.Mobileno = responseData.Mobileno;
                    responseViewModel.Coins = responseData.Coins;
                    responseViewModel.City = responseData.City;
                    responseViewModel.DOB = responseData.DOB;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(SaveUserRegistration);
                string ErrorSource = "Controller:INCUSController.cs;Method:Incus_VerifyLogin;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("logoutAPI", Name = "Incus_Logout")]
        [HttpPost]
        public async Task<ResponseViewModel> Incus_Logout(LogoutModel Emailid)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            try

            {
                var responseData = await _connectedRepo.logout(Emailid);
                if (responseData != null && responseData.State == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(Emailid);
                string ErrorSource = "Controller:INCUSController.cs;Method:logout;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        #endregion

        #region MasterData
        [Route("GetExamCategoryDetails", Name = "GetExamCategoryDetails")]
        [HttpGet]
        public async Task<GetExamCategoryDetailsViewModel> GetExamCategoryDetails()
        {
            GetExamCategoryDetailsViewModel model = new GetExamCategoryDetailsViewModel();
            try
            {
                var res = await _connectedRepo.GetExamCategoryDetails();


                model = new GetExamCategoryDetailsViewModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: GetExamCategoryDetails;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("GetExamSubCategoryDetails", Name = "GetExamSubCategoryDetails")]
        [HttpPost]
        public async Task<GetSubCategoryDetailsViewModel> GetExamSubCategoryDetails(INCUSSubCategoryRequest CategoryID)
        {
            GetSubCategoryDetailsViewModel model = new GetSubCategoryDetailsViewModel();
            try
            {
                var res = await _connectedRepo.GetExamSubCategoryDetails(CategoryID);


                model = new GetSubCategoryDetailsViewModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: GetExamSubCategoryDetails;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("GetTournamentSearchPin", Name = "GetTournamentSearchPin")]
        [HttpPost]
        public async Task<GetSubCategoryDetailsViewModel> GetTournamentSearchPin(GetTournamentSearchPin PinData)
        {
            GetSubCategoryDetailsViewModel model = new GetSubCategoryDetailsViewModel();
            try
            {
                var res = await _connectedRepo.GetTournamentSearchPin(PinData);


                model = new GetSubCategoryDetailsViewModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: GetTournamentSearchPin;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        #endregion

        #region Game Play
        [Route("RegisterForGame", Name = "RegisterForGame")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> RegisterForGame(RegisterforGameInput saveRegisterforGameReq)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.RegisterForGame(saveRegisterforGameReq);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                    responseViewModel.Gamesplayed = responseData.Gamesplayed;
                    responseViewModel.ExpiryDate = responseData.ExpiryDate;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(saveRegisterforGameReq);
                string ErrorSource = "Controller:INCUSController.cs;Method:RegisterForGame;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        // quiz game questions API
        [Route("GetQuizGameQuestions")]
        [HttpPost]
        public async Task<UserPackageTestQuestionsResponse> GetQuizGameQuestions(AcceptChallengeReq acceptChallengeReq)
        {
            UserPackageTestQuestionsResponse resp = new UserPackageTestQuestionsResponse();

            try
            {
                var res = await _connectedRepo.GetQuizGameQuestions(acceptChallengeReq);
                resp.ResponseId = res.Item2;
                resp.Message = res.Item3;

                if (res != null && res.Item2 == 1)
                {
                    resp.userTestQuestions = res.Item1;
                }
                else
                {
                    resp.userTestQuestions = new UserTestQuestions();
                }
                resp.State = true;
                resp.Message = res.Item3;
                resp.MusicFile = res.Item4;
                return resp;
            }
            catch (Exception ex)
            {
                resp.Message = WebAPIResourceFile.SomethingWentWrong;
                resp.userTestQuestions = new UserTestQuestions();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(acceptChallengeReq);
                string ErrorSource = "Controller: QuizGameController.cs;Method: GetQuizGameQuestions;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }

        [Route("SaveAnswerForGame", Name = "SaveAnswerForGame")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> SaveAnswerForGame(AnswerforQuestion verifyOtp)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                //Incus.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(verifyOtp);
                //string ErrorSource = "Controller:INCUSController.cs;Method:SaveAnswerForGame;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, null);

                var responseData = await _connectedRepo.SaveAnswerForGame(verifyOtp);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(verifyOtp);
                string ErrorSource = "Controller:INCUSController.cs;Method:SaveAnswerForGame;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("BargraphContent/{QuestionCode}")]
        [HttpPost]
        public async Task<BargraphResponse> BargraphContent(string QuestionCode)
        {
            BargraphResponse resp = new BargraphResponse();

            try
            {

                //Incus.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(QuestionCode);
                //string ErrorSource = "Controller:INCUSController.cs;Method:BargraphContent;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, null);

                //Task t = Task.Run(() => {
                //    Task.Delay(5000).Wait();
                //  //  Console.WriteLine("Task ended delay...");
                //});
                var res = await _connectedRepo.BargraphContent(QuestionCode);
                resp = res;
                return resp;
            }
            catch (Exception ex)
            {
                //  resp.Message = WebAPIResource.SomethingWentWrong;
                //  resp.userTestQuestions = new UserTestQuestions();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject("test");
                string ErrorSource = "Controller: QuizGameController.cs;Method: BargraphContent;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }

        [Route("GetLeaderboardDashboard")]
        [HttpPost]
        public async Task<LeaderboardResponseModel> INCUS_GetLeaderboardDashboard(LeaderboardRequest QuestionCode)
        {
            LeaderboardResponseModel resp = new LeaderboardResponseModel();

            try
            {
                var res = await _connectedRepo.INCUS_GetLeaderboardDashboard(QuestionCode);
                resp.loaderboardresponse = res.Item1;
                resp.TotalPlayers = res.Item2;
                return resp;
            }
            catch (Exception ex)
            {
                //  resp.Message = WebAPIResource.SomethingWentWrong;
                //  resp.userTestQuestions = new UserTestQuestions();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject("test");
                string ErrorSource = "Controller: QuizGameController.cs;Method: INCUS_GetLeaderboardDashboard;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }
        #endregion
        #region Wallet
        [Route("SavePaymentTransaction", Name = "SavePaymentTransaction")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> SavePaymentTransaction(PaymentModel verifyOtp)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.SavePaymentTransaction(verifyOtp);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(verifyOtp);
                string ErrorSource = "Controller:INCUSController.cs;Method:SavePaymentTransaction;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("FinalResult")]
        [HttpPost]
        public async Task<FinalResultResponseViewModel> FinalResult(RegisterforGameInput FinalInput)
        {

            FinalResultResponseViewModel resp = new FinalResultResponseViewModel();

            try
            {
                var res = await _connectedRepo.FinalResult(FinalInput);
                resp = new FinalresultResponseModel(res);
                return resp;
            }
            catch (Exception ex)
            {
                //  resp.Message = WebAPIResource.SomethingWentWrong;
                //  resp.userTestQuestions = new UserTestQuestions();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject("test");
                string ErrorSource = "Controller: QuizGameController.cs;Method: FinalResult;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }

        [Route("PlayerHistory/{Phoneno}")]
        [HttpGet]
        public async Task<GetHistoryDetailsViewModel> INCUS_PlayerHistory(string Phoneno)
        {

            GetHistoryDetailsViewModel resp = new GetHistoryDetailsViewModel();

            try
            {
                var res = await _connectedRepo.INCUS_PlayerHistory(Phoneno);
                resp = new GetHistoryDetailsViewModel(res);
                return resp;
            }
            catch (Exception ex)
            {
                //  resp.Message = WebAPIResource.SomethingWentWrong;
                //  resp.userTestQuestions = new UserTestQuestions();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject("test");
                string ErrorSource = "Controller: QuizGameController.cs;Method: INCUS_PlayerHistory;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }
        #endregion

        // quiz game questions API
        [Route("GetReviewQuizGameQuestions")]
        [HttpPost]
        public async Task<UserPackageTestQuestionsbargraphResponse> GetReviewQuizGameQuestions(AcceptChallengeReq acceptChallengeReq)
        {
            UserPackageTestQuestionsbargraphResponse resp = new UserPackageTestQuestionsbargraphResponse();

            try
            {
                var res = await _connectedRepo.GetReviewQuizGameQuestions(acceptChallengeReq);
                resp.ResponseId = res.Item2;
                resp.Message = res.Item3;

                if (res != null && res.Item2 == 1)
                {
                    resp.userTestQuestions = res.Item1;
                }
                else
                {
                    resp.userTestQuestions = new UserTestQuestionswithbookmark();
                }
                resp.State = true;
                resp.Message = res.Item3;
                return resp;
            }
            catch (Exception ex)
            {
                resp.Message = WebAPIResourceFile.SomethingWentWrong;
                resp.userTestQuestions = new UserTestQuestionswithbookmark();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(acceptChallengeReq);
                string ErrorSource = "Controller: QuizGameController.cs;Method: GetReviewQuizGameQuestions;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }

        [Route("PaymentSubscriptionDetails/{Phoneno}", Name = "PaymentSubscriptionDetails")]
        [HttpGet]
        public async Task<PaymentSubscriptionModel> PaymentSubscriptionDetails(string Phoneno)
        {
            PaymentSubscriptionModel responseViewModel = new PaymentSubscriptionModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.PaymentSubscriptionDetails(Phoneno);
                if (responseData != null)
                {
                    responseViewModel = responseData;
                }
                else
                {
                    responseViewModel = responseData;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.CurrentPaymentAmount = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(Phoneno);
                string ErrorSource = "Controller:INCUSController.cs;Method:PaymentSubscriptionDetails;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return responseViewModel;
        }

        [Route("UserNotifications/Phoneno", Name = "UserNotifications")]
        [HttpGet]
        public async Task<GameNotificationModel> UserNotifications(string Phoneno)
        {
            GameNotificationModel model = new GameNotificationModel();
            try
            {
                var res = await _connectedRepo.UserNotifications(Phoneno);


                model = new GameNotificationModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: UserNotifications;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }


        [Route("INCUSSampleNotification/{Phoneno}", Name = "INCUSSampleNotification")]
        [HttpPost]
        public async Task<ResponseViewModel> INCUSSampleNotification(string Phoneno)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            try
            {
                var responseData = await _connectedRepo.INCUSSampleNotification(Phoneno);
                if (responseData != null && responseData.Status == "1")
                {
                    responseViewModel.State = true;
                }
                responseViewModel.Message = responseData.Message;
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(Phoneno);
                string ErrorSource = "Controller:ConnectedCareController.cs;Method:SampleNotification;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }


        [Route("VersionAPI", Name = "Incus_VersionAPI")]
        [HttpGet]
        public async Task<VersionResponseViewModel> Incus_VersionAPI()
        {
            VersionResponseViewModel responseViewModel = new VersionResponseViewModel();
            try

            {
                var responseData = await _connectedRepo.Incus_VersionAPI();
                if (responseData != null && responseData.State == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                    responseViewModel.Version = responseData.Version;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject("sam");
                string ErrorSource = "Controller:INCUSController.cs;Method:VersionAPI;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }
        #region Reports
        [Route("ReportUsers", Name = "Incus_ReportUsers")]
        [HttpGet]
        public async Task<ReportUsersModel> Incus_ReportUsers()
        {
            ReportUsersModel model = new ReportUsersModel();
            try
            {
                var res = await _connectedRepo.Incus_ReportUsers();


                model = new ReportUsersModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: Incus_ReportUsers;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        #endregion

        #region Bookmarks
        [Route("SaveBookMark", Name = "SaveBookMark")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> SaveBookMark(BookmarkRequest bookmarkRequest)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.SaveBookMark(bookmarkRequest);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(bookmarkRequest);
                string ErrorSource = "Controller:INCUSController.cs;Method:SaveAnswerForGame;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("GetBookmarks/{Phoneno}", Name = "Incus_GetBookmarks")]
        [HttpGet]
        public async Task<GetBookmarksRequestModel> Incus_GetBookmarks(string Phoneno)
        {
            GetBookmarksRequestModel model = new GetBookmarksRequestModel();
            try
            {
                var res = await _connectedRepo.Incus_GetBookmarks(Phoneno);
                model = new GetBookmarksRequestModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: Incus_GetBookmarks;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("GetQuestionBookmarkReview")]
        [HttpPost]
        public async Task<UserPackageTestQuestionsbargraphResponse> GetQuestionBookmarkReview(BookmarkRequest QuestionCode)
        {
            UserPackageTestQuestionsbargraphResponse resp = new UserPackageTestQuestionsbargraphResponse();

            try
            {
                var res = await _connectedRepo.GetQuestionBookmarkReview(QuestionCode);
                resp.ResponseId = res.Item2;
                resp.Message = res.Item3;

                if (res != null && res.Item2 == 1)
                {
                    resp.userTestQuestions = res.Item1;
                }
                else
                {
                    resp.userTestQuestions = new UserTestQuestionswithbookmark();
                }
                resp.State = true;
                resp.Message = res.Item3;
                return resp;
            }
            catch (Exception ex)
            {
                resp.Message = WebAPIResourceFile.SomethingWentWrong;
                resp.userTestQuestions = new UserTestQuestionswithbookmark();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(QuestionCode);
                string ErrorSource = "Controller: QuizGameController.cs;Method: GetQuestionBookmarkReview;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }

        [Route("SaveComments", Name = "SaveComments")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> SaveComments(CommentRequest commentRequest)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.SaveComments(commentRequest);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(commentRequest);
                string ErrorSource = "Controller:INCUSController.cs;Method:SaveAnswerForGame;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("GetQuestionComments/{QuestionCode}", Name = "GetQuestionComments")]
        [HttpGet]
        public async Task<GetCommentsViewModel> GetQuestionComments(string QuestionCode)
        {
            GetCommentsViewModel model = new GetCommentsViewModel();
            try
            {
                var res = await _connectedRepo.GetQuestionComments(QuestionCode);
                model = new GetCommentsViewModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: GetQuestionComments;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("SaveReportComments", Name = "SaveReportComments")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> SaveReportComments(CommentReport commentRequest)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.SaveReportComments(commentRequest);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(commentRequest);
                string ErrorSource = "Controller:INCUSController.cs;Method:SaveReportComments;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("GetQuestionCommentsforReport", Name = "Incus_GetQuestionCommentsforReport")]
        [HttpGet]
        public async Task<GetCommentslistofwebModel> Incus_GetQuestionCommentsforReport()
        {
            GetCommentslistofwebModel model = new GetCommentslistofwebModel();
            try
            {
                var res = await _connectedRepo.Incus_GetQuestionCommentsforReport();
                model = new GetCommentslistofwebModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: Incus_GetQuestionCommentsforReport;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("RepublishComment", Name = "Incus_RepublishComment")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> Incus_RepublishComment(RepublishComment CommentPublish)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.Incus_RepublishComment(CommentPublish);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(CommentPublish);
                string ErrorSource = "Controller:INCUSController.cs;Method:Incus_RepublishComment;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("GetQuestionDetails")]
        [HttpPost]
        public async Task<UserPackageTestQuestionsbargraphResponse> GetQuestionDetails(BookmarkRequest QuestionCode)
        {
            UserPackageTestQuestionsbargraphResponse resp = new UserPackageTestQuestionsbargraphResponse();

            try
            {
                var res = await _connectedRepo.GetQuestionDetails(QuestionCode);
                resp.ResponseId = res.Item2;
                resp.Message = res.Item3;

                if (res != null && res.Item2 == 1)
                {
                    resp.userTestQuestions = res.Item1;
                }
                else
                {
                    resp.userTestQuestions = new UserTestQuestionswithbookmark();
                }
                resp.State = true;
                resp.Message = res.Item3;
                return resp;
            }
            catch (Exception ex)
            {
                resp.Message = WebAPIResourceFile.SomethingWentWrong;
                resp.userTestQuestions = new UserTestQuestionswithbookmark();
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(QuestionCode);
                string ErrorSource = "Controller: QuizGameController.cs;Method: GetQuestionBookmarkReview;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return resp;
        }

        #endregion

        #region Quizquestionsforweb
        [Route("GetQuizQuestionsAll")]
        [HttpPost]
        public async Task<QuestionforWebModel> GetQuizQuestionsAll(QuestionSearchRequest Searchkey)
        {

            QuestionforWebModel model = new QuestionforWebModel();
            try
            {
                var res = await _connectedRepo.GetQuizQuestionsAll(Searchkey);
                model.QuestionsList = res.Item1;
                model.TotalQuestions = res.Item2;
            }
            catch (Exception ex)
            {

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: GetQuizQuestionsAll;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("UpdateSubCategoryName", Name = "UpdateSubCategoryName")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> UpdateSubCategoryName(UpdateTournamentName tournamentName)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.UpdateSubCategoryName(tournamentName);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(tournamentName);
                string ErrorSource = "Controller:INCUSController.cs;Method:UpdateSubCategoryName;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("UpdateQuestionbyRecord", Name = "UpdateQuestionbyRecord")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> UpdateQuestionbyRecord(QuestionforWeb questionrecord)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.UpdateQuestionbyRecord(questionrecord);
                if (responseData != null && responseData.Status == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.Status;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(questionrecord);
                string ErrorSource = "Controller:INCUSController.cs;Method:UpdateQuestionbyRecord;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("GetCorrectwrongCounts", Name = "GetCorrectwrongCounts")]
        [HttpPost]
        public async Task<GetCorrectwrongCountsReponseModel> GetCorrectwrongCounts(GetCorrectwrongCountsRequest QuestionCode)
        {
            GetCorrectwrongCountsReponseModel model = new GetCorrectwrongCountsReponseModel();
            try
            {
                var res = await _connectedRepo.GetCorrectwrongCounts(QuestionCode);
                model = new GetCorrectwrongCountsReponseModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: GetCorrectwrongCounts;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }
        #endregion

        #region 7th Nov 2020

        [Route("Incus_SaveCourses", Name = "Incus_SaveCourses")]
        [HttpPost]
        public async Task<INCUSBaseResponseModel> Incus_SaveCourses(Courses courseDetails)
        {
            INCUSBaseResponseModel responseViewModel = new INCUSBaseResponseModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.Incus_SaveCourses(courseDetails);
                if (responseData != null && responseData.State == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.State;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.Status = responseData.State;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(courseDetails);
                string ErrorSource = "Controller:INCUSController.cs;Method:Incus_SaveCourses;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("GetCorrectwrongCounts", Name = "GetCorrectwrongCounts")]
        [HttpGet]
        public async Task<CoursesResponseModel> Incus_GetCourses(string UserID)
        {
            CoursesResponseModel model = new CoursesResponseModel();
            try
            {
                var res = await _connectedRepo.Incus_GetCourses(UserID);
                model = new CoursesResponseModel(res);
                model.Message = WebAPIResourceFile.ListFound;
                model.State = true;
            }
            catch (Exception ex)
            {
                model.Message = WebAPIResourceFile.SomethingWentWrong;

                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                string ErrorSource = "Controller: INCUSController.cs;Method: Incus_GetCourses;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return model;
        }

        [Route("SaveTournament", Name = "Incus_SaveTournament")]
        [HttpPost]
        public async Task<ResponseViewModel> Incus_SaveTournament(INCUSSubcategoryPL tournamentDetails)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.INCUS_SaveSubCategory(tournamentDetails);
                if (responseData != null && responseData.State == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(tournamentDetails);
                string ErrorSource = "Controller:INCUSController.cs;Method:INCUS_SaveSubCategory;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        [Route("SaveQuestions", Name = "Incus_SaveQuestions")]
        [HttpPost]
        public async Task<ResponseViewModel> Incus_SaveQuestions(SaveQuestionPL questionDetails)
        {
            ResponseViewModel responseViewModel = new ResponseViewModel();
            string TempPatientID = string.Empty;
            try

            {
                var responseData = await _connectedRepo.INCUS_SaveQuestion(questionDetails);
                if (responseData != null && responseData.State == true)
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
                else
                {
                    responseViewModel.Message = responseData.Message;
                    responseViewModel.State = responseData.State;
                }
            }
            catch (Exception ex)
            {
                responseViewModel.Message = WebAPIResourceFile.SomethingWentWrong;
                Incus.Repository.ExceptionLogger _exceptionLogger = null;
                string jsonrequest = string.Empty;
                jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(questionDetails);
                string ErrorSource = "Controller:INCUSController.cs;Method:Incus_SaveQuestions;InputRequest=" + jsonrequest;
                _exceptionLogger = new Incus.Repository.ExceptionLogger(new DBLogger());
                await _exceptionLogger.LogException(ErrorSource, ex);
            }

            return responseViewModel;
        }

        #endregion


        #region Important SQL STEPS for updating the question options in master table 

        //        Declare @questioncode as nvarchar(30)
        //Declare @OptionA as nvarchar(200)
        //Declare @OptionB as nvarchar(200)
        //Declare @OptionC as nvarchar(200)
        //Declare @OptionD as nvarchar(200)
        //Declare @Count as int
        //select @Count=Count(questioncode) from incus_Quizquestions where optionA is Null
        //print @Count
        //while @count > 0
        //Begin
        //select Top(1) @Questioncode =Questioncode from incus_Quizquestions where optionA is Null order by Createddate desc
        //select @OptionA = Answeroption from incus_QuizMapQuestionsToAnswers where questioncode =@Questioncode and options = 'A'
        //select @OptionB = Answeroption from incus_QuizMapQuestionsToAnswers where questioncode = @Questioncode and options = 'B'
        //select @OptionC = Answeroption from incus_QuizMapQuestionsToAnswers where questioncode = @Questioncode and options = 'C'
        //select @OptionD = Answeroption from incus_QuizMapQuestionsToAnswers where questioncode = @Questioncode and options = 'D'
        //update incus_quizquestions set OptionA = @OptionA, OptionB = @OptionB, OptionC = @OptionC, OptionD = @OptionD where questioncode = @Questioncode
        //set @count = @count - 1
        //End

        #endregion


    }
}

