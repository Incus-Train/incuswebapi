﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AzureCloud.Core
{
    public class ContinuationToken
    {
        public static string Serialize(TableContinuationToken token)
        {
            string serialized = null;
            if (token != null)
            {
                using (var writer = new StringWriter())
                {
                    using (var xmlWriter = XmlWriter.Create(writer))
                    {
                        token.WriteXml(xmlWriter);
                    }
                    serialized = writer.ToString();
                }
            }

            return serialized;
        }

        public static TableContinuationToken Deserialize(string token)
        {
            TableContinuationToken contToken = null;
            if (!string.IsNullOrWhiteSpace(token))
            {
                using (var stringReader = new StringReader(token))
                {
                    contToken = new TableContinuationToken();
                    using (var xmlReader = XmlReader.Create(stringReader))
                    {
                        contToken.ReadXml(xmlReader);
                    }
                }
            }

            return contToken;
        }
    }
}
