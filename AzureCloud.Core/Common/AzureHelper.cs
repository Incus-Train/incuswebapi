﻿using AzureCloud.Core.Common;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incus.Core.Common;

namespace AzureCloud.Core.Common
{
    public class AzureHelper
    {
        public static string GetAzureCloudStorageConnectionStringBasedOnDeploymentType()
        {
            var DeploymentType = CoreHelper.GetDeploymentType();

            if (DeploymentType.ToLower().Contains("live") || DeploymentType.ToLower().Contains("prod"))
            {
                return ConfigurationManager.AppSettings["StorageConnectionStringFor_Prodction"] != null ? ConfigurationManager.AppSettings["StorageConnectionStringFor_Prodction"] : "DefaultEndpointsProtocol=https;AccountName=click2clinicstorage;AccountKey=h2vYSrGebfZcV8XsxC++nqO1JfTQ6Z8hpzi1BGVln2S2VqzUTa5oBkvEvsydLeCvp0LMQTdz6YSy6/OuQXjnEA==";
            }
            else
            {
                return ConfigurationManager.AppSettings["StorageConnectionStringFor_Development"] != null ? ConfigurationManager.AppSettings["StorageConnectionStringFor_Development"] : "DefaultEndpointsProtocol=https;AccountName=click2clinicstoragedev;AccountKey=z4mldtKlrIWd83XDA1gRL8HyA12zPQKgrY0n7bqVDXHDOw2DNkOWnJVRJflw3cnX2cfBdE/bzPBi1xT4wiXB9w==";
            }
        }

        public static CloudStorageAccount GetCloudStorageAccountObjectBasedOnDeploymentType()
        {
            var azureCloudStorageconnectionString = GetAzureCloudStorageConnectionStringBasedOnDeploymentType();
            return CloudStorageAccount.Parse(azureCloudStorageconnectionString);
        }

        public static CloudStorageAccount GetMasterDataCloudStorageAccount()
        {
            var azureCloudStorageconnectionString = ConfigurationManager.AppSettings["StorageConnectionStringFor_Prodction"] != null ? ConfigurationManager.AppSettings["StorageConnectionStringFor_Prodction"] : "DefaultEndpointsProtocol=https;AccountName=click2clinicstorage;AccountKey=h2vYSrGebfZcV8XsxC++nqO1JfTQ6Z8hpzi1BGVln2S2VqzUTa5oBkvEvsydLeCvp0LMQTdz6YSy6/OuQXjnEA=="; ;
            return CloudStorageAccount.Parse(azureCloudStorageconnectionString);
        }
    }
}
