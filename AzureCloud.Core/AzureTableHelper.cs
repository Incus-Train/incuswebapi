﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Core
{

    public class AzureTableHelper
    {
        public static CloudTable GetCloudTable(string tableName, bool createIfNotExists = false)
        {
            var storageAccount = CloudStorageAccount.Parse(Config.Get("C2CStorageAccount"));
            var tableClient = storageAccount.CreateCloudTableClient();
            var table = tableClient.GetTableReference(tableName);
            if (createIfNotExists)
            {
                table.CreateIfNotExists();
            }
            return table;
        }
    }
}
