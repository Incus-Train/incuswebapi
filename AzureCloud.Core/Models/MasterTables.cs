﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;

namespace AzureCloud.Core
{
    //partition key ChatId
    //row key timestamp
    public class MasterTables : TableEntity
    {
        public string ConnectionId { get; set; }
        public string SessionId { get; set; }
        public string SessionType { get; set; } //User or Group
        public string UserId { get; set; } // user sent the chat
        public string Name { get; set; }
        public string SentUtc { get; set; }
        public string MessageType { get; set; }
        public string Message { get; set; }

    }

    public class AsureSubjectDetails : TableEntity
    {
        public string SubjectCode { get; set; }
        public string SubjectName { get; set; }
    }

    public class SubjectsEntity : TableEntity
    {
        public SubjectsEntity(string SubjectCode, string SubjectName, string SubjectDescription, string CreatedDate, string Createdby, string LastModifiedDate, string LastModifiedby, string SubImage)
        {
            this.PartitionKey = "C2C";
            this.RowKey = SubjectCode;
        }
        public SubjectsEntity() { }
        public string SubjectCode { get; set; }
        public Int32 SubjectIdForSort { get; set; }
        public string SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDescription { get; set; }
        public string CreatedDate { get; set; }
        public string Createdby { get; set; }
        public string LastModifiedDate { get; set; }
        public string LastModifiedby { get; set; }
        public string Image { get; set; }


    }

    public class CourseInfo : TableEntity
    {
        public string CourseName { get; set; }
        public string MapCode { get; set; }
        public string CourseDescription { get; set; }
        public string CourseCode { get; set; }
        public string VideoLink { get; set; }
        public string CourseImage { get; set; }
        public string CourseId { get; set; }

    }
    public class SearchUserInfo : TableEntity
    {
        public string UserMobileNo { get; set; }
        public string TwitterId { get; set; }
        public string Name { get; set; }

        public string EmailId { get; set; }

        public string gender { get; set; }

        public string image { get; set; }

        public string age { get; set; }

        public string subjectcode { get; set; }

        public string uservalue { get; set; }
    }

    public class UserRegistrationEntity : TableEntity
    {
        public UserRegistrationEntity(string Phoneno)
        {
            this.PartitionKey = "Clinicopedia India HIMS";
            this.RowKey = Phoneno;
        }
        public UserRegistrationEntity() { }
        public string id { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Phoneno { get; set; }
        public string Password { get; set; }
        public string usertype { get; set; }
        public string noofvisits { get; set; }
        public string status { get; set; }
        public string lastlogindatetime { get; set; }
        public string createddatetime { get; set; }
        public string gcmid { get; set; }
        public string projectid { get; set; }
        public string uservalue { get; set; }
        public string otp { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string gender { get; set; }
        public string image { get; set; }
        public string subjectcode { get; set; }
        public string age { get; set; }
        public string selfDescription { get; set; }
        public string Country { get; set; }
        public string city { get; set; }
        public string Patientid { get; set; }
        public string Fbid { get; set; }
        public string gmailtoken { get; set; }
        public string tipsstatus { get; set; }
        public string UserMobileNo { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string TwitterId { get; set; }
        public string LinkedIn { get; set; }
        public string RefferalCode { get; set; }
    }
}
