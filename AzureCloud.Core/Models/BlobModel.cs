﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Core.Models
{
    public class BlobUploadModel
    {
        public string SubDirectory { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public long FileSizeInBytes { get; set; }
        public long FileSizeInKb { get { return (long)Math.Ceiling((double)FileSizeInBytes / 1024); } }

        public string ContentType { get; set; }
    }
    public class BlobDownloadModel
    {
        public MemoryStream BlobStream { get; set; }
        public string BlobFileName { get; set; }
        public string BlobContentType { get; set; }
        public long BlobLength { get; set; }
    }
    public class UploadViewModel
    {
        public string FileName { get; set; }
        public string Message { get; set; }
        public bool State { get; set; }
        public string UploadedURL { get; set; }
    }
    public class FileMetaData
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class FileViewModel
    {

        public string BlobContainerName { get; set; }
        public byte[] FileContent { get; set; }
        public Stream FileStream { get; set; }
        public IEnumerable<FileMetaData> MetaDataList { get; set; }
        public string Name { get; set; }
        public string SubDirectory { get; set; }
    }
}
