﻿using Microsoft.Azure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCloud.Core
{
    public class CloudConfigurationHelper
    {
        /// <summary>
        /// Same as RoleEnvironmentHelper.GetConfigurationSettingValue()
        /// but returns String.Empty instead of throwing an exception
        /// </summary>
        /// <param name="setting">The name of the configuration setting.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>A string containing the value of the configuration setting, or a null reference if the specified setting was not found.</returns>
        public static string GetSetting(string setting, string defaultValue = null)
        {
            try
            {
                return CloudConfigurationManager.GetSetting(setting);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            { }
            return defaultValue;
        }
    }
}
