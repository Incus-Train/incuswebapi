﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incus.Models
{
    public class LoginRequestModel
    {
        public string LoginID { get; set; }
        public string Password { get; set; }
    }
    public class LoginResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Phoneno { get; set; }
    }
    public class SaveCommentsResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
    public class ExceptionLogEntity
    {
        public string requestObj { get; set; }
        public string errorMsg { get; set; }
        public string innerMessage { get; set; }
        public string stackTrace { get; set; }
        public string appName { get; set; }

    }
    public class FinalResultResponseViewModel
    {
        public bool State { get; set; }
        public string Message { get; set; }
    }
    public class VersionResponseViewModel
    {
        public string Version { get; set; }
        public bool State { get; set; }
        public string Message { get; set; }
    }
    public class ResponseViewModel
    {
        public bool State { get; set; }
        public string Message { get; set; }
    }
    public class LogoutModel
    {
        public string emailid { get; set; }
    }
    public class INCUSSubCategoryRequest
    {
        public string Emailid { get; set; }
        public string CategoryID { get; set; }
        public string PlayType { get; set; }
    }
    public class PaymentSubscriptionModel
    {
        public string CurrentPaymentDate { get; set; }
        public string CurrentPaymentAmount { get; set; }
        public string RenewalPaymentDate { get; set; }
        public string RenewalPaymentAmount { get; set; }
    }
    public class GetTournamentSearchPin
    {
        public string Phoneno { get; set; }
        public string Pinnumber { get; set; }
    }
    public class INCUSBaseResponseModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Mobileno { get; set; }
        public string Coins { get; set; }
        public string City { get; set; }
        public string Gamesplayed { get; set; }
        public string DOB { get; set; }
        public string ExpiryDate { get; set; }
    }
    public class INCUSRegisterModel
    {
        public string Name { get; set; }
        public string Phoneno { get; set; }
        public string Emailid { get; set; }
        public string Password { get; set; }
        public string Registerfrom { get; set; }
        public string City { get; set; }
        public string DOB { get; set; }
        public string GCMID { get; set; }
        public string ProfilePic { get; set; }
    }

    public class INCUSVerifyUserModel
    {
        public string MailID { get; set; }
    }
    public class RegisterforGameInput
    {
        public string Phoneno { get; set; }
        public string CategoryID { get; set; }
        public string SubCategoryID { get; set; }
    }
    public class AnswerforQuestion
    {
        public string Phoneno { get; set; }
        public string CategoryID { get; set; }
        public string SubCategoryid { get; set; }
        public string QuestionCode { get; set; }
        public string UserAnswer { get; set; }
        public string AnswerStatus { get; set; }
        public string TimeTaken { get; set; }
    }
    public class PaymentModel
    {
        public string Phoneno { get; set; }
        public string PaymentTransactionID { get; set; }
        public string BankTransactionID { get; set; }
        public string BankResponseCode { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public string Amount { get; set; }
    }
    public class RegisterResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string RegisterCode { get; set; }
    }
    public class GetExamCategoryDetailsViewModel : ResponseViewModel
    {
        private List<ExamCategoryList> _examCategoryListDetails = new List<ExamCategoryList>();

        public GetExamCategoryDetailsViewModel()
        {
        }
        public GetExamCategoryDetailsViewModel(List<ExamCategoryList> ExamCategoryListDetails)
        {
            _examCategoryListDetails = ExamCategoryListDetails;
        }

        public List<ExamCategoryList> ExamCategoryMasterLsit { get { return _examCategoryListDetails; } }
    }
    public class ExamCategoryList
    {
        public string CategoryName { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string CategoryImageURL { get; set; }
        public string CategoryID { get; set; }
    }
    public class GetSubCategoryDetailsViewModel : ResponseViewModel
    {
        private List<SubCategoryList> _examSubCategoryListDetails = new List<SubCategoryList>();

        public GetSubCategoryDetailsViewModel()
        {
        }
        public GetSubCategoryDetailsViewModel(List<SubCategoryList> ExamSubCategoryListDetails)
        {
            _examSubCategoryListDetails = ExamSubCategoryListDetails;
        }

        public List<SubCategoryList> ExamSubCategoryMasterLsit { get { return _examSubCategoryListDetails; } }
    }
    public class SubCategoryList
    {
        public string SubCategoryID { get; set; }
        public string SubCategoryName { get; set; }
        public string Timeslots { get; set; }
        public string SponsoredBy { get; set; }
        public string SubCategoryImageURL { get; set; }
        public string CategoryID { get; set; }
        public string TimeforEachQuestion { get; set; }
        public string RegisterStatus { get; set; }
        public string TotalRegistered { get; set; }
        public string Prizevalue { get; set; }
        public string TournamentDate { get; set; }
        public string RemainingTime { get; set; }
        public string Pinnumber { get; set; }
    }
    public class INCUSSubcategoryPL
    {
        public string CategoryID { get; set; }
        public string CourseID { get; set; }
        public string SubcategoryID { get; set; }
        public string SubCategoryName { get; set; }
        public string Description { get; set; }
        public string SponsoredBy { get; set; }
        public string PrizeOffered { get; set; }
        public string Notification { get; set; }
        public string TypeofTournament { get; set; }
        public string TeacherDrivenType { get; set; }
        public string DateofGame { get; set; }
        public string TimeSlots { get; set; }
        public string Timezone { get; set; }
        public string SubCategoryImageURL { get; set; }
        public string TournamentStatus { get; set; }
        public string Createdby { get; set; }

    }
    public class AcceptChallengeReq
    {
        public string CategoryId { get; set; }
        public string SubCategoryID { get; set; }
        public string Phoneno { get; set; }
    }
    public class UserPackageTestQuestionsResponse : ResponseViewModel
    {
        public UserTestQuestions userTestQuestions { get; set; }
        public int ResponseId { get; set; }
        public string MusicFile { get; set; }
    }
    public class UserPackageTestQuestionsbargraphResponse : ResponseViewModel
    {
        public UserTestQuestionswithbookmark userTestQuestions { get; set; }
        public int ResponseId { get; set; }
    }
    public enum UMedicoTestStatus
    {
        Initiated = 1,
        Completed = 2,
        Pending = 3,
        TimedOut = 4
    };
    public class UserTestQuestions
    {
        public List<Question> questionList { get; set; }
        public int RightAnswerCount { get; set; }
        public int WrongAnswerCount { get; set; }
        public int UnAnsweredCount { get; set; }
        public int CompletedTime { get; set; }
        public UMedicoTestStatus TestStatus { get; set; }
        public int Rank { get; set; }
        public DateTime TestAttendeddate { get; set; }
        public string ExamRank { get; set; }
        public string TotalAttendedStudentExams { get; set; }
        public int PracticeTestTotalStudentAttend { get; set; }
        public string VideoHeadCode { get; set; }
        public string VedicoCourseCode { get; set; }
    }
    public class UserTestQuestionswithbookmark
    {
        public List<Questionbargraph> questionList { get; set; }
        public int RightAnswerCount { get; set; }
        public int WrongAnswerCount { get; set; }
        public int UnAnsweredCount { get; set; }
        public int CompletedTime { get; set; }
        public UMedicoTestStatus TestStatus { get; set; }
        public int Rank { get; set; }
        public DateTime TestAttendeddate { get; set; }
        public string ExamRank { get; set; }
        public string TotalAttendedStudentExams { get; set; }
        public int PracticeTestTotalStudentAttend { get; set; }
        public string VideoHeadCode { get; set; }
        public string VedicoCourseCode { get; set; }
    }
    public class Question
    {
        public string QuestionID { get; set; }
        public string QuestionText { get; set; }
        public List<string> Options { get; set; }
        public string ActualAnswer { get; set; }
        public string UserAnswer { get; set; }
        public string Images { get; set; }
        public string AnswerStatus { get; set; }
    }
    public class Questionbargraph
    {
        public string QuestionID { get; set; }
        public string QuestionText { get; set; }
        public List<string> Options { get; set; }
        public string ActualAnswer { get; set; }
        public string UserAnswer { get; set; }
        public string Images { get; set; }
        public string AnswerStatus { get; set; }
        public string BookmarkStatus { get; set; }
        public string BookmarkCount { get; set; }
        public string OptionACount { get; set; }
        public string OptionBCount { get; set; }
        public string OptionCCount { get; set; }
        public string OptionDCount { get; set; }
        public string Explanation { get; set; }
    }
    public class BargraphResponse
    {
        public string QuestionText { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public string OptionACount { get; set; }
        public string OptionBCount { get; set; }
        public string OptionCCount { get; set; }
        public string OptionDCount { get; set; }
        public string CorrectAnswer { get; set; }
    }
    public class LeaderboardResponseModel
    {
        public string TotalPlayers { get; set; }
        public List<LeaderboardResponse> loaderboardresponse { get; set; }
    }
    public class LeaderboardRequest
    {
        public string QuestionCode { get; set; }
        public string Phoneno { get; set; }
    }
    public class LeaderboardResponse
    {
        public string Name { get; set; }
        public string Marks { get; set; }
        public string Bonus { get; set; }
        public string TotalMarks { get; set; }
        public string TimeTaken { get; set; }
        public string TotalScore { get; set; }
    }

    public class FinalresultResponseforUser
    {
        public string Rank { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string UserID { get; set; }
        public string CorrectAnswers { get; set; }
        public string TotalCount { get; set; }
        public string ProfilePic { get; set; }
    }
    public class FinalresultResponse
    {
        public string Ranks { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string UserID { get; set; }
        public string CorrectAnswers { get; set; }
        public string TotalCount { get; set; }
        public string ProfilePic { get; set; }
    }
    public class FinalresultResponseModel : FinalResultResponseViewModel
    {
        private List<FinalresultResponse> _finalResponseModel = new List<FinalresultResponse>();

        public FinalresultResponseModel()
        {
        }
        public FinalresultResponseModel(List<FinalresultResponse> finalResponseModelDetails)
        {
            _finalResponseModel = finalResponseModelDetails;
        }

        public List<FinalresultResponse> finalResponseModel { get { return _finalResponseModel; } }
    }
    public class GetHistoryDetailsViewModel : ResponseViewModel
    {
        private List<HistoryList> _historyMasterListDetails = new List<HistoryList>();

        public GetHistoryDetailsViewModel()
        {
        }
        public GetHistoryDetailsViewModel(List<HistoryList> HistoryMasterLsitDetails)
        {
            _historyMasterListDetails = HistoryMasterLsitDetails;
        }

        public List<HistoryList> HistoryMasterLsit { get { return _historyMasterListDetails; } }
    }
    public class HistoryList
    {
        public string CategoryId { get; set; }
        public string CategoryImageURL { get; set; }
        public string Subcategoryid { get; set; }
        public string CategoryName { get; set; }
        public string SubcategoryName { get; set; }
        public string SponsoredBy { get; set; }
        public string ActualScore { get; set; }
        public string TotalCount { get; set; }
        public string TotalPlayers { get; set; }
        public string DateofTournament { get; set; }
        public string SubcategoryImageURL { get; set; }
        public string Timeslots { get; set; }
    }

    public class GameNotificationModel : ResponseViewModel
    {
        private List<GameNotification> _notificationDetails = new List<GameNotification>();

        public GameNotificationModel()
        {
        }
        public GameNotificationModel(List<GameNotification> NotificationListDetails)
        {
            _notificationDetails = NotificationListDetails;
        }

        public List<GameNotification> GetNotificationList { get { return _notificationDetails; } }
    }
    public class GameNotification
    {
        public string NotificationID { get; set; }
        public string Notification { get; set; }
    }
    public class ReportUsersModel : ResponseViewModel
    {
        private List<ReportUsersList> _ReportUsersList = new List<ReportUsersList>();

        public ReportUsersModel()
        {
        }
        public ReportUsersModel(List<ReportUsersList> _reportUsersList)
        {
            _ReportUsersList = _reportUsersList;
        }

        public List<ReportUsersList> reportUsersList { get { return _ReportUsersList; } }
    }
    public class ReportUsersList
    {
        public string Registeredon { get; set; }
        public string Name { get; set; }
        public string Emailid { get; set; }
        public string Phoneno { get; set; }
        public string City { get; set; }
        public string DOB { get; set; }
        public string SubscriptionType { get; set; }
        public string RecentPaymentDate { get; set; }
        public string PaymentType { get; set; }
        public string CouponCode { get; set; }
        public string SubscriptionStartDate { get; set; }
        public string SubscriptionEndDate { get; set; }
        public string ExamsPreparing { get; set; }
        public string GamesPlayed { get; set; }
    }
    public class BookmarkRequest
    {
        public string Phoneno { get; set; }
        public string QuestionCode { get; set; }
    }
    public class GetBookmarksResponse
    {
        public string QuestionCode { get; set; }
        public string QuestionText { get; set; }
        public string CorrectAnswer { get; set; }
        public string UserAnswer { get; set; }
        public string BookmarkCount { get; set; }
    }
    public class GetBookmarksRequestModel : ResponseViewModel
    {
        private List<GetBookmarksResponse> _GetBookmarksResponse = new List<GetBookmarksResponse>();

        public GetBookmarksRequestModel()
        {
        }
        public GetBookmarksRequestModel(List<GetBookmarksResponse> _getBookmarksResponse)
        {
            _GetBookmarksResponse = _getBookmarksResponse;
        }

        public List<GetBookmarksResponse> getBookmarksResponseList { get { return _GetBookmarksResponse; } }
    }
    public class CommentRequest
    {
        public string Phoneno { get; set; }
        public string QuestionCode { get; set; }
        public string Comments { get; set; }
        public string ImageUrl { get; set; }
    }
    public class GetCommentsViewModel : ResponseViewModel
    {
        private List<GetCommentsList> _commentsListDetails = new List<GetCommentsList>();

        public GetCommentsViewModel()
        {
        }
        public GetCommentsViewModel(List<GetCommentsList> ExamCategoryListDetails)
        {
            _commentsListDetails = ExamCategoryListDetails;
        }

        public List<GetCommentsList> CommentsList { get { return _commentsListDetails; } }
    }
    public class GetCommentsList
    {
        public string Name { get; set; }
        public string Profilepic { get; set; }
        public string CommentTimestamp { get; set; }
        public string Comments { get; set; }
        public string Commentimageurl { get; set; }
        public string CommentID { get; set; }
    }
    public class CommentReport
    {
        public string Commentid { get; set; }
        public string QuestionCode { get; set; }
        public string Phoneno { get; set; }
    }
    public class GetCommentslistofweb
    {
        public string Commenttext { get; set; }
        public string Commentimage { get; set; }
        public string Commentatorname { get; set; }
        public string Commentatoremailid { get; set; }
        public string CommentattorMobileno { get; set; }
        public string Mcqcontent { get; set; }
        public string Mcqanswer { get; set; }
        public string Game { get; set; }
        public string Gamecategory { get; set; }
        public string Pinnumber { get; set; }
        public string Reportername { get; set; }
        public string Reportermobileno { get; set; }
        public string Publishstatus { get; set; }
        public string Commenteddate { get; set; }
        public string Reporteremailid { get; set; }
        public string CommentID { get; set; }
        public string ReportID { get; set; }
    }
    public class GetCommentslistofwebModel : ResponseViewModel
    {
        private List<GetCommentslistofweb> _commentsListDetails = new List<GetCommentslistofweb>();

        public GetCommentslistofwebModel()
        {
        }
        public GetCommentslistofwebModel(List<GetCommentslistofweb> ExamCategoryListDetails)
        {
            _commentsListDetails = ExamCategoryListDetails;
        }

        public List<GetCommentslistofweb> CommentsList { get { return _commentsListDetails; } }
    }
    public class RepublishComment
    {
        public string Commentid { get; set; }
        public string PublishStatus { get; set; }
    }
    public class QuestionCountResponseViewModel
    {
        public bool State { get; set; }
        public string Message { get; set; }
        public string TotalCount { get; set; }
    }
    public class QuestionforWeb
    {
        public string QuestionCode { get; set; }
        public string CategoryName { get; set; }
        public string TournamentName { get; set; }
        public string TournamentPinnumber { get; set; }
        public string QuestionID { get; set; }
        public string QuestionText { get; set; }
        public string ActualAnswer { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        //     public string OptionE { get; set; }
        public string QuestionIMAGEURL { get; set; }
        public string QuestionAudioURL { get; set; }
        public string QuestionVideoURL { get; set; }
        public string ExplanationText { get; set; }
        public string ExplanationImage1 { get; set; }
        public string ExplanationImage2 { get; set; }
        public string ExplanationImage3 { get; set; }
        public string ExplanationImage4 { get; set; }
        public string ExplanationImage5 { get; set; }
        public string ExplanationImage6 { get; set; }
        public string ExplanationImage7 { get; set; }
        public string ExplanationImage8 { get; set; }
        public string ExplanationImage9 { get; set; }
        public string ExplanationImage10 { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
    }
    public class QuestionforWebModel
    {
        public string TotalQuestions { get; set; }
        public List<QuestionforWeb> QuestionsList { get; set; }
    }
    public class UpdateTournamentName
    {
        public string Pinnumber { get; set; }
        public string TournamentName { get; set; }
    }
    public class QuestionSearchRequest
    {
        public string FromCount { get; set; }
        public string ToCount { get; set; }
        public string SearchKey { get; set; }
    }
    public class GetCorrectwrongCountsRequest
    {
        public string Phoneno { get; set; }
        public string AnswerStatus { get; set; }
    }
    public class GetCorrectwrongCountsReponseModel : ResponseViewModel
    {
        private List<GetCorrectwrongCountsReponse> _commentsListDetails = new List<GetCorrectwrongCountsReponse>();

        public GetCorrectwrongCountsReponseModel()
        {
        }
        public GetCorrectwrongCountsReponseModel(List<GetCorrectwrongCountsReponse> ExamCategoryListDetails)
        {
            _commentsListDetails = ExamCategoryListDetails;
        }

        public List<GetCorrectwrongCountsReponse> CommentsList { get { return _commentsListDetails; } }
    }
    public class GetCorrectwrongCountsReponse
    {
        public string Questioncode { get; set; }
        public string Questionname { get; set; }
        public string Categoryname { get; set; }
        public string Subcategoryname { get; set; }
        public string TotalResult { get; set; }
        public string Totalanswered { get; set; }
        public string Bookmarkstatus { get; set; }
        public string ActualAnswer { get; set; }
        public string UserAnswer { get; set; }
    }
    public class CoursesResponseModel : ResponseViewModel
    {
        private List<Courses> _commentsListDetails = new List<Courses>();

        public CoursesResponseModel()
        {
        }
        public CoursesResponseModel(List<Courses> ExamCategoryListDetails)
        {
            _commentsListDetails = ExamCategoryListDetails;
        }

        public List<Courses> CoursesList { get { return _commentsListDetails; } }
    }
    public class Courses
    {
        public string CourseID { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public string AboutAuthor { get; set; }
        public string CategoryID { get; set; }
        public string TotalMCQs { get; set; }
        public string TotalTournaments { get; set; }
        public string Price { get; set; }
        public string SubscriptionDuration { get; set; }
        public string CreatedBy { get; set; }
        public string CourseImageURL { get; set; }
    }
    public class SaveQuestionRequest
    {
        public string QuestionText { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public string AboutAuthor { get; set; }
        public string CategoryID { get; set; }
        public string TotalMCQs { get; set; }
        public string TotalTournaments { get; set; }
        public string Price { get; set; }
        public string SubscriptionDuration { get; set; }
        public string CreatedBy { get; set; }
        public string CourseImageURL { get; set; }
    }
    public class SaveQuestionPL
    {
        public string Questionname { get; set; }
        public string Answer { get; set; }
        public string Createdby { get; set; }
        public string Subcategoryid { get; set; }
        public string Categoryid { get; set; }
        public string Explanation { get; set; }
        public string Questionimageurl { get; set; }
        public string Explanation1 { get; set; }
        public string Explanation2 { get; set; }
        public string Explanation3 { get; set; }
        public string Explanation4 { get; set; }
        public string Explanation5 { get; set; }
        public string Explanation6 { get; set; }
        public string Explanation7 { get; set; }
        public string Explanation8 { get; set; }
        public string Explanation9 { get; set; }
        public string Explanation10 { get; set; }
        public string Topic { get; set; }
        public string Subject { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public string OptionE { get; set; }
        public string QuestionVideoURL { get; set; }
        public string QuestionAudioURL { get; set; }
        public string Questiontime { get; set; }
        public string Courseid { get; set; }
    }
}
