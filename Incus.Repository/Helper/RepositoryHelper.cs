﻿using Incus.Datalayer;
using Incus.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Incus.Repository.Helper
{
    public class RepositoryHelper
    {
        public static string GetVerificationCode()
        {
            var r = new Random();
            var sessionId = r.Next(100000, 999999).ToString();
            return sessionId;
        }
        public static string GetRandomNumberCodeUp6Digits()
        {
            var r = new Random();
            var sessionId = r.Next(100000, 999999).ToString();
            return sessionId;
        }
        public static string GetApplicationRefNo(string second)
        {
            string ApplicationRefNo = second;

            try
            {
                long third = (Convert.ToInt64(second) + 1);
                if (third < 10)
                    ApplicationRefNo = "0000000000" + third;
                else if (third < 100 && third > 9)
                    ApplicationRefNo = "000000000" + third;
                else if (third < 1000 && third > 99)
                    ApplicationRefNo = "00000000" + third;
                else if (third < 10000 && third > 999)
                    ApplicationRefNo = "0000000" + third;
                else if (third < 100000 && third > 9999)
                    ApplicationRefNo = "000000" + third;
                else if (third < 1000000 && third > 99999)
                    ApplicationRefNo = "00000" + third;
                else if (third < 10000000 && third > 999999)
                    ApplicationRefNo = "0000" + third;
                else if (third < 100000000 && third > 9999999)
                    ApplicationRefNo = "000" + third;
                else if (third < 1000000000 && third > 99999999)
                    ApplicationRefNo = "00" + third;
            }
            catch (Exception ex)
            {
            }
            return ApplicationRefNo;
        }
        public static List<string> GetOptionsBasedOnQuestionCode(string questionCode, DataTable dataTable)
        {
            List<string> answers = new List<string>();
            DataView dv = new DataView(dataTable);
            dv.RowFilter = "questionCode='" + questionCode + "'";
            DataTable dt = dv.ToTable();
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (!string.IsNullOrEmpty(dt.Rows[i]["AnswerOption"].ToString()))
                    answers.Add(dt.Rows[i]["options"].ToString() + ") " + dt.Rows[i]["AnswerOption"].ToString());
            }
            return answers;
        }
        public static List<string> GetImagesBasedOnQuestionCode(string imageUrl, string questionCode, DataTable dataTable)
        {
            List<string> images = new List<string>();
            DataView dv = new DataView(dataTable);
            dv.RowFilter = "questionCode='" + questionCode + "'";
            DataTable dt = dv.ToTable();
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (!string.IsNullOrEmpty(dt.Rows[i]["ImageName"].ToString()))
                    images.Add(imageUrl + dt.Rows[i]["ImageName"].ToString());
            }
            return images;
        }

    }
}
