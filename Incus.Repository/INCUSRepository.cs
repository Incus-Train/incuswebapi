﻿using Incus.Datalayer;
using Incus.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using AzureCloud.Data;
using System.Configuration;
using Incus.Core.Common;
using Incus.Repository.Helper;
using System.Net.Http;

namespace Incus.Repository
{
    public class INCUSRepository
    {
        IncusDal objconnectedcaredal = new IncusDal();
        private IAzureUserRepository _azureUserRepository;
        public string GetApplicationRefNo(string second)
        {
            try
            {
                string ApplicationRefNo = second;
                long third = (Convert.ToInt64(second) + 1);
                if (third < 10)
                    ApplicationRefNo = "00000000" + third;
                else if (third < 100 && third > 9)
                    ApplicationRefNo = "0000000" + third;
                else if (third < 1000 && third > 99)
                    ApplicationRefNo = "000000" + third;
                else if (third < 10000 && third > 999)
                    ApplicationRefNo = "00000" + third;
                else if (third < 100000 && third > 9999)
                    ApplicationRefNo = "0000" + third;
                else if (third < 1000000 && third > 99999)
                    ApplicationRefNo = "000" + third;
                else if (third < 10000000 && third > 999999)
                    ApplicationRefNo = "00" + third;
                else if (third < 100000000 && third > 9999999)
                    ApplicationRefNo = "0" + third;
                return ApplicationRefNo;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Userlogin and profile
        public async Task<LoginResponseModel> INCUS_VerifyOTPWeb(string UserID, string Password)
        {
            LoginResponseModel rd = new LoginResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.INCUS_VerifyOTPWeb(UserID, Password);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                rd.Message = ds.Tables[0].Rows[0]["ResponseText"].ToString().TrimEnd();
                rd.Status = ds.Tables[0].Rows[0]["ResponseID"].ToString().TrimEnd();
                rd.Role = ds.Tables[0].Rows[0]["typeofhcp"].ToString().TrimEnd();
                rd.Name = ds.Tables[0].Rows[0]["Name"].ToString().TrimEnd();
                rd.Phoneno = ds.Tables[0].Rows[0]["phoneno"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<INCUSBaseResponseModel> SaveIncusUserRegistration(INCUSRegisterModel SaveUserRegistration)
        {
            INCUSBaseResponseModel objnewres = new INCUSBaseResponseModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.SaveIncusUserRegistration(SaveUserRegistration);
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.Status = true;
                }
                else
                {
                    objnewres.Status = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            else
            {
                objnewres.Status = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        public async Task<INCUSBaseResponseModel> UpdatePhoneno(INCUSRegisterModel SaveUserRegistration)
        {
            INCUSBaseResponseModel objnewres = new INCUSBaseResponseModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.UpdatePhoneno(SaveUserRegistration);
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.Status = true;
                }
                else
                {
                    objnewres.Status = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
                objnewres.Mobileno = ds.Tables[0].Rows[0]["Phoneno"].ToString().TrimEnd();
            }
            else
            {
                objnewres.Status = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        public async Task<INCUSBaseResponseModel> Incus_VerifyLogin(INCUSVerifyUserModel SaveUserRegistration)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.Incus_VerifyLogin(SaveUserRegistration);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
                rd.Mobileno = ds.Tables[0].Rows[0]["Phoneno"].ToString().TrimEnd();
                rd.City = ds.Tables[0].Rows[0]["City"].ToString().TrimEnd();
                rd.Coins = ds.Tables[0].Rows[0]["coins"].ToString().TrimEnd();
                rd.DOB = ds.Tables[0].Rows[0]["DOB"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<ResponseViewModel> logout(LogoutModel Emailid)
        {
            ResponseViewModel objnewres = new ResponseViewModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.logout(Emailid);
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.State = true;
                }
                else
                {
                    objnewres.State = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            else
            {
                objnewres.State = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        #endregion
        #region MasterData
        public async Task<List<ExamCategoryList>> GetExamCategoryDetails()
        {
            List<ExamCategoryList> patientBankDetails = new List<ExamCategoryList>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.GetExamCategoryDetails();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("CategoryID"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new ExamCategoryList
                    {
                        CategoryID = ds.Tables[0].Rows[i]["CategoryID"].ToString().Trim(),
                        CategoryName = ds.Tables[0].Rows[i]["CategoryName"].ToString().Trim(),
                        Line1 = ds.Tables[0].Rows[i]["Line1"].ToString().Trim(),
                        Line2 = ds.Tables[0].Rows[i]["Line2"].ToString().Trim(),
                        CategoryImageURL = "https://incusblobstorage.blob.core.windows.net/imageresources/category/" + ds.Tables[0].Rows[i]["CategoryImageURL"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<List<SubCategoryList>> GetExamSubCategoryDetails(INCUSSubCategoryRequest req)
        {
            List<SubCategoryList> patientBankDetails = new List<SubCategoryList>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.GetExamSubCategoryDetails(req);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("CategoryID"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new SubCategoryList
                    {
                        CategoryID = ds.Tables[0].Rows[i]["CategoryID"].ToString().Trim(),
                        SubCategoryID = ds.Tables[0].Rows[i]["SubCategoryID"].ToString().Trim(),
                        SubCategoryName = ds.Tables[0].Rows[i]["SubCategoryName"].ToString().Trim(),
                        Timeslots = ds.Tables[0].Rows[i]["Timeslots"].ToString().Trim(),
                        SponsoredBy = ds.Tables[0].Rows[i]["SponsoredBy"].ToString().Trim(),
                        SubCategoryImageURL = "https://incusblobstorage.blob.core.windows.net/imageresources/tournament/" + ds.Tables[0].Rows[i]["SubCategoryImageURL"].ToString().Trim(),
                        TimeforEachQuestion = ds.Tables[0].Rows[i]["timeforeachquestion"].ToString().Trim(),
                        RegisterStatus = ds.Tables[0].Rows[i]["Registerstatus"].ToString().Trim(),
                        TournamentDate = ds.Tables[0].Rows[i]["dateofTournament"].ToString().Trim(),
                        TotalRegistered = ds.Tables[0].Rows[i]["TotalRegistered"].ToString().Trim(),
                        RemainingTime = ds.Tables[0].Rows[i]["RemainingTime"].ToString().Trim(),
                        Prizevalue = ds.Tables[0].Rows[i]["Prizevalue"].ToString().Trim(),
                        Pinnumber = ds.Tables[0].Rows[i]["pinnumber"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<List<SubCategoryList>> GetTournamentSearchPin(GetTournamentSearchPin PinData)
        {
            List<SubCategoryList> patientBankDetails = new List<SubCategoryList>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.GetTournamentSearchPin(PinData);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("subcategoryid"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new SubCategoryList
                    {
                        CategoryID = ds.Tables[0].Rows[i]["CategoryID"].ToString().Trim(),
                        SubCategoryID = ds.Tables[0].Rows[i]["SubCategoryID"].ToString().Trim(),
                        SubCategoryName = ds.Tables[0].Rows[i]["SubCategoryName"].ToString().Trim(),
                        Timeslots = ds.Tables[0].Rows[i]["Timeslots"].ToString().Trim(),
                        SponsoredBy = ds.Tables[0].Rows[i]["SponsoredBy"].ToString().Trim(),
                        SubCategoryImageURL = "https://incusblobstorage.blob.core.windows.net/imageresources/tournament/" + ds.Tables[0].Rows[i]["SubCategoryImageURL"].ToString().Trim(),
                        TimeforEachQuestion = ds.Tables[0].Rows[i]["timeforeachquestion"].ToString().Trim(),
                        RegisterStatus = ds.Tables[0].Rows[i]["Registerstatus"].ToString().Trim(),
                        TotalRegistered = ds.Tables[0].Rows[i]["TotalPlayers"].ToString().Trim(),
                        Prizevalue = ds.Tables[0].Rows[i]["Prizevalue"].ToString().Trim(),
                        TournamentDate = ds.Tables[0].Rows[i]["dateoftournament"].ToString().Trim(),
                        RemainingTime = ds.Tables[0].Rows[i]["RemainingTime"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        #endregion
        public async Task<INCUSBaseResponseModel> RegisterForGame(RegisterforGameInput savePatientImageReq)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.RegisterforGame(savePatientImageReq);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
                rd.Gamesplayed = ds.Tables[0].Rows[0]["gamesplayed"].ToString().TrimEnd();
                rd.ExpiryDate = ds.Tables[0].Rows[0]["expirydate"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<Tuple<UserTestQuestions, int, string, string>> GetQuizGameQuestions(AcceptChallengeReq acceptChallengeReq)
        {
            UserTestQuestions userMockTestQuestions = new UserTestQuestions();
            DataSet ds = new DataSet();
            List<Question> questionList = new List<Question>();

            ds = await objconnectedcaredal.GetQuizGameQuestions(acceptChallengeReq);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("ResponseId") && ds.Tables[0].Rows[0]["ResponseId"].ToString() == "1")
            {
                var imageUrl = CoreHelper.AzureCloudUmedicoQuizQuestionImageDownloadURL();

                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    questionList.Add(new Question
                    {
                        QuestionID = dr["QuestionCode"].ToString().TrimEnd(),
                        QuestionText = dr["QuestionName"].ToString().TrimEnd(),
                        Options = ds != null && ds.Tables.Count > 1 && ds != null && ds.Tables[1].Rows.Count > 0 ? RepositoryHelper.GetOptionsBasedOnQuestionCode(dr["questioncode"].ToString(), ds.Tables[2]) : new List<string>(),
                        Images = dr["QuestionImageUrl"].ToString(),
                        ActualAnswer = dr["Answer"].ToString().TrimEnd()
                    });
                }
                userMockTestQuestions.questionList = questionList;
                userMockTestQuestions.TestStatus = UMedicoTestStatus.Initiated;
            }
            return new Tuple<UserTestQuestions, int, string, string>(userMockTestQuestions, Convert.ToInt32(ds.Tables[0].Rows[0]["ResponseId"].ToString()), ds.Tables[0].Rows[0]["ResponseText"].ToString(), "https://incusblobstorage.blob.core.windows.net/musicfiles/" + ds.Tables[0].Rows[0]["musicfilename"].ToString());
        }
        public async Task<INCUSBaseResponseModel> SaveAnswerForGame(AnswerforQuestion verifyOtp)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.SaveAnswerForGame(verifyOtp);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<BargraphResponse> BargraphContent(string QuestionCode)
        {
            BargraphResponse rd = new BargraphResponse();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.INCUS_GetBargraphContent(QuestionCode);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                rd.QuestionText = ds.Tables[2].Rows[0]["QuestionName"].ToString().TrimEnd();
                rd.CorrectAnswer = ds.Tables[2].Rows[0]["CorrectAnswer"].ToString().TrimEnd();
                rd.OptionACount = ds.Tables[0].Rows[0]["A"].ToString().TrimEnd();
                if (rd.OptionACount == string.Empty)
                    rd.OptionACount = "0";
                rd.OptionA = ds.Tables[1].Rows[0]["A"].ToString().TrimEnd();
                rd.OptionBCount = ds.Tables[0].Rows[0]["B"].ToString().TrimEnd();
                if (rd.OptionBCount == string.Empty)
                    rd.OptionBCount = "0";
                rd.OptionB = ds.Tables[1].Rows[0]["B"].ToString().TrimEnd();
                rd.OptionCCount = ds.Tables[0].Rows[0]["C"].ToString().TrimEnd();
                if (rd.OptionCCount == string.Empty)
                    rd.OptionCCount = "0";
                rd.OptionC = ds.Tables[1].Rows[0]["C"].ToString().TrimEnd();
                rd.OptionDCount = ds.Tables[0].Rows[0]["D"].ToString().TrimEnd();
                if (rd.OptionDCount == string.Empty)
                    rd.OptionDCount = "0";
                rd.OptionD = ds.Tables[1].Rows[0]["D"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<Tuple<List<LeaderboardResponse>, string>> INCUS_GetLeaderboardDashboard(LeaderboardRequest QuestionCode)
        {
            List<LeaderboardResponse> rd = new List<LeaderboardResponse>();
            string TotalPlayers = string.Empty;
            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.INCUS_GetLeaderboardDashboard(QuestionCode);
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                rd.Add(new LeaderboardResponse
                {
                    Name = dr["Name"].ToString().TrimEnd(),
                    Marks = dr["Marks"].ToString().TrimEnd(),
                    Bonus = dr["Bonus"].ToString().TrimEnd(),
                    TotalMarks = dr["TotalMarks"].ToString().TrimEnd(),
                    TimeTaken = dr["TimeTaken"].ToString().TrimEnd(),
                    TotalScore = dr["TotalScore"].ToString().TrimEnd(),

                });
            }
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                TotalPlayers = ds.Tables[0].Rows[0]["TotalPlayers"].ToString().TrimEnd();
            }

            return new Tuple<List<LeaderboardResponse>, string>(rd, TotalPlayers);

        }
        public async Task<List<FinalresultResponse>> FinalResult(RegisterforGameInput QuestionCode)
        {
            List<FinalresultResponse> rd = new List<FinalresultResponse>();
            List<FinalresultResponseforUser> rd1 = new List<FinalresultResponseforUser>();
            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.FinalResult(QuestionCode);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    rd.Add(new FinalresultResponse
                    {

                        Ranks = ds.Tables[0].Rows[i]["Ranks"].ToString().TrimEnd(),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString().TrimEnd(),
                        City = ds.Tables[0].Rows[i]["City"].ToString().TrimEnd(),
                        CorrectAnswers = ds.Tables[0].Rows[i]["CorrectAnswers"].ToString().TrimEnd(),
                        TotalCount = ds.Tables[0].Rows[i]["totalcount"].ToString().TrimEnd(),
                        ProfilePic = ds.Tables[0].Rows[i]["ProfilePic"].ToString().TrimEnd(),
                    });

                }
                for (int i = 0; i <= ds.Tables[1].Rows.Count - 1; i++)
                {
                    rd1.Add(new FinalresultResponseforUser
                    {
                        Rank = ds.Tables[1].Rows[i]["Ranks"].ToString().TrimEnd(),
                        Name = ds.Tables[1].Rows[i]["Name"].ToString().TrimEnd(),
                        City = ds.Tables[1].Rows[i]["City"].ToString().TrimEnd(),
                        CorrectAnswers = ds.Tables[1].Rows[i]["CorrectAnswers"].ToString().TrimEnd(),
                        TotalCount = ds.Tables[1].Rows[i]["totalcount"].ToString().TrimEnd(),
                        ProfilePic = ds.Tables[1].Rows[i]["ProfilePic"].ToString().TrimEnd(),
                    });
                }
            }
            return rd;
        }
        public async Task<List<HistoryList>> INCUS_PlayerHistory(string Phoneno)
        {
            List<HistoryList> rd = new List<HistoryList>();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.INCUS_PlayerHistory(Phoneno);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    rd.Add(new HistoryList
                    {
                        CategoryId = ds.Tables[0].Rows[i]["CategoryId"].ToString().TrimEnd(),
                        CategoryImageURL = "https://incusblobstorage.blob.core.windows.net/imageresources/tournament/" + ds.Tables[0].Rows[i]["CategoryImageURL"].ToString().TrimEnd(),
                        CategoryName = ds.Tables[0].Rows[i]["CategoryName"].ToString().TrimEnd(),
                        Subcategoryid = ds.Tables[0].Rows[i]["Subcategoryid"].ToString().TrimEnd(),
                        SubcategoryImageURL = "https://incusblobstorage.blob.core.windows.net/imageresources/tournament/" + ds.Tables[0].Rows[i]["SubcategoryImageURL"].ToString().TrimEnd(),
                        SubcategoryName = ds.Tables[0].Rows[i]["SubcategoryName"].ToString().TrimEnd(),
                        SponsoredBy = ds.Tables[0].Rows[i]["SponsoredBy"].ToString().TrimEnd(),
                        ActualScore = ds.Tables[0].Rows[i]["ActualScore"].ToString().TrimEnd(),
                        TotalCount = ds.Tables[0].Rows[i]["TotalCount"].ToString().TrimEnd(),
                        TotalPlayers = ds.Tables[0].Rows[i]["TotalRegistered"].ToString().TrimEnd(),
                        DateofTournament = ds.Tables[0].Rows[i]["Dateoftournament"].ToString().TrimEnd(),
                        Timeslots = ds.Tables[0].Rows[i]["Timeslots"].ToString().TrimEnd(),
                    });
                }
            }
            return rd;
        }
        #region Wallet
        public async Task<INCUSBaseResponseModel> SavePaymentTransaction(PaymentModel verifyOtp)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.SavePaymentTransaction(verifyOtp);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        #endregion
        public async Task<Tuple<UserTestQuestionswithbookmark, int, string>> GetReviewQuizGameQuestions(AcceptChallengeReq acceptChallengeReq)
        {
            UserTestQuestionswithbookmark userMockTestQuestions = new UserTestQuestionswithbookmark();
            DataSet ds = new DataSet();
            List<Questionbargraph> questionList = new List<Questionbargraph>();

            ds = await objconnectedcaredal.GetReviewQuizGameQuestions(acceptChallengeReq);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("ResponseId") && ds.Tables[0].Rows[0]["ResponseId"].ToString() == "1")
            {
                var imageUrl = CoreHelper.AzureCloudUmedicoQuizQuestionImageDownloadURL();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    questionList.Add(new Questionbargraph
                    {
                        QuestionID = dr["QuestionCode"].ToString().TrimEnd(),
                        QuestionText = dr["QuestionName"].ToString().TrimEnd(),
                        Images = dr["questionimageurl"].ToString().TrimEnd(),
                        Options = ds != null && ds.Tables.Count > 1 && ds != null && ds.Tables[1].Rows.Count > 0 ? RepositoryHelper.GetOptionsBasedOnQuestionCode(dr["questioncode"].ToString(), ds.Tables[2]) : new List<string>(),
                        //Images = ds != null && ds.Tables.Count > 2 && ds != null && ds.Tables[2].Rows.Count > 0 ? RepositoryHelper.GetImagesBasedOnQuestionCode(imageUrl, dr["questioncode"].ToString(), ds.Tables[3]) : new List<string>(),
                        ActualAnswer = dr["Answer"].ToString().TrimEnd(),
                        UserAnswer = dr["useranswer"].ToString().TrimEnd(),
                        AnswerStatus = dr["AnswerStatus"].ToString().TrimEnd(),
                        BookmarkStatus = dr["BookmarkStatus"].ToString().TrimEnd(),
                        BookmarkCount = dr["BookmarkCount"].ToString().TrimEnd(),
                        OptionACount = !string.IsNullOrEmpty(dr["A"].ToString().TrimEnd()) ? dr["A"].ToString().TrimEnd() : "0",
                        OptionBCount = !string.IsNullOrEmpty(dr["B"].ToString().TrimEnd()) ? dr["B"].ToString().TrimEnd() : "0",
                        OptionCCount = !string.IsNullOrEmpty(dr["C"].ToString().TrimEnd()) ? dr["C"].ToString().TrimEnd() : "0",
                        OptionDCount = !string.IsNullOrEmpty(dr["D"].ToString().TrimEnd()) ? dr["D"].ToString().TrimEnd() : "0",
                        Explanation = dr["Explanation"].ToString().TrimEnd(),
                    });
                }
                userMockTestQuestions.questionList = questionList;
                userMockTestQuestions.TestStatus = UMedicoTestStatus.Initiated;
            }
            return new Tuple<UserTestQuestionswithbookmark, int, string>(userMockTestQuestions, Convert.ToInt32(ds.Tables[0].Rows[0]["ResponseId"].ToString()), ds.Tables[0].Rows[0]["ResponseText"].ToString());
        }

        public async Task<PaymentSubscriptionModel> PaymentSubscriptionDetails(string Phoneno)
        {
            PaymentSubscriptionModel rd = new PaymentSubscriptionModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.PaymentSubscriptionDetails(Phoneno);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                rd.CurrentPaymentDate = ds.Tables[0].Rows[0]["Paymentdate"].ToString().TrimEnd();
                rd.CurrentPaymentAmount = ds.Tables[0].Rows[0]["PAymentamount"].ToString().TrimEnd();
                rd.RenewalPaymentDate = ds.Tables[0].Rows[0]["Renewaldate"].ToString().TrimEnd();
                rd.RenewalPaymentAmount = ds.Tables[0].Rows[0]["RenewalAmount"].ToString().TrimEnd();
            }
            return rd;

        }

        public async Task<List<GameNotification>> UserNotifications(string Phoneno)
        {
            List<GameNotification> patientBankDetails = new List<GameNotification>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.UserNotifications(Phoneno);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("NotificationID"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new GameNotification
                    {
                        NotificationID = ds.Tables[0].Rows[i]["NotificationID"].ToString().Trim(),
                        Notification = ds.Tables[0].Rows[i]["Notification"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<SaveCommentsResponseModel> INCUSSampleNotification(string Phoneno)
        {

            SaveCommentsResponseModel objnewres = new SaveCommentsResponseModel();
            //DataSet ds = new DataSet();

            //ds = await objconnectedcaredal.INCUSSampleNotification(Phoneno);
            //if (ds != null && ds.Tables.Count > 0)
            //{
            //    objnewres.Status = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
            //    objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            //    if (objnewres.Status == "1")
            //    {
            string GCMID = string.Empty;
            string UserValue = string.Empty;
            string PatientName = string.Empty;
            string C2CExecutiveName = string.Empty;
            string MatchMakerName = string.Empty;
            string HCPName = string.Empty;
            string HospitalId = string.Empty;
            string MatchMakerNumber = string.Empty;

            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    GCMID = ds.Tables[0].Rows[0]["GCMID"].ToString().TrimEnd();
            //    UserValue = ds.Tables[0].Rows[0]["UserValue"].ToString().TrimEnd();
            //    PatientName = ds.Tables[0].Rows[0]["PatientName"].ToString().TrimEnd();
            //    C2CExecutiveName = ds.Tables[0].Rows[0]["C2CExecutiveName"].ToString().TrimEnd();
            //    string Messageoutput = "Dear " + PatientName + " your request has been assigned to " + C2CExecutiveName + "";
            //          var res = CoreHelper.SendNotificationTest("dKe4l1GjSG6h17e67h929-:APA91bHrGmUVD-J39oJw4bS7EteJMJSEvlBVJa3Ts7FCwl8vNob9_KamAPNuRMeRx_dtPwGa3Rodvpnbk-bDAPL9ZpBq51kDXnluANE8aEldEIHmKl6flLVrLOMmp0p08WLZE_rpJHbK","Sample test");
            //     CoreHelper.Notifications();

            CoreHelper.NotifyAsync("eATTFJvXRZ-JHe-UbmLepB:APA91bGM_6IJJ_sfj6ivHCu2Fl3QS4escirwA0QUqdcsRZcX_Jcn_OoRcRoxoW2n9qvxueLG-eJAYJz7T-7OEytjDulMKTtNtriEYL368RKqXfuWzIcIa4VOX-bViy5RcjOs-6G3i_wD", "hello", "I am Anil");
            //       }


            //        objnewres.Status = objnewres.Status;
            //        objnewres.Message = objnewres.Message;
            //    }
            //    else
            //    {
            //        objnewres.Status = objnewres.Status;
            //        objnewres.Message = objnewres.Message;
            //    }
            //}
            //else
            //{
            //    objnewres.Status = "0";
            //    objnewres.Message = "Already Exists";
            //}
            return objnewres;

        }
        public async Task<VersionResponseViewModel> Incus_VersionAPI()
        {
            VersionResponseViewModel objnewres = new VersionResponseViewModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.Incus_VersionAPI();
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.State = true;
                }
                else
                {
                    objnewres.State = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
                objnewres.Version = ds.Tables[0].Rows[0]["VersionAPI"].ToString().TrimEnd();
            }
            else
            {
                objnewres.State = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        #region Reports
        public async Task<List<ReportUsersList>> Incus_ReportUsers()
        {
            List<ReportUsersList> patientBankDetails = new List<ReportUsersList>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.Incus_ReportUsers();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("Registeredon"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new ReportUsersList
                    {
                        Registeredon = ds.Tables[0].Rows[i]["Registeredon"].ToString().Trim(),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString().Trim(),
                        Emailid = ds.Tables[0].Rows[i]["Emailid"].ToString().Trim(),
                        Phoneno = ds.Tables[0].Rows[i]["Phoneno"].ToString().Trim(),
                        City = ds.Tables[0].Rows[i]["City"].ToString().Trim(),
                        DOB = ds.Tables[0].Rows[i]["DOB"].ToString().Trim(),
                        SubscriptionType = ds.Tables[0].Rows[i]["SubscriptionType"].ToString().Trim(),
                        RecentPaymentDate = ds.Tables[0].Rows[i]["RecentPaymentDate"].ToString().Trim(),
                        PaymentType = ds.Tables[0].Rows[i]["PaymentType"].ToString().Trim(),
                        CouponCode = ds.Tables[0].Rows[i]["CouponCode"].ToString().Trim(),
                        SubscriptionStartDate = ds.Tables[0].Rows[i]["SubscriptionStartDate"].ToString().Trim(),
                        SubscriptionEndDate = ds.Tables[0].Rows[i]["SubscriptionEndDate"].ToString().Trim(),
                        ExamsPreparing = ds.Tables[0].Rows[i]["ExamsPreparing"].ToString().Trim(),
                        GamesPlayed = ds.Tables[0].Rows[i]["gamesplayed"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        #endregion

        #region Bookmark
        public async Task<INCUSBaseResponseModel> SaveBookMark(BookmarkRequest bookmarkRequest)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.SaveBookMark(bookmarkRequest);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<List<GetBookmarksResponse>> Incus_GetBookmarks(string Phoneno)
        {
            List<GetBookmarksResponse> patientBankDetails = new List<GetBookmarksResponse>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.Incus_GetBookmarks(Phoneno);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("questioncode"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new GetBookmarksResponse
                    {
                        QuestionCode = ds.Tables[0].Rows[i]["QuestionCode"].ToString().Trim(),
                        QuestionText = ds.Tables[0].Rows[i]["QuestionText"].ToString().Trim(),
                        CorrectAnswer = ds.Tables[0].Rows[i]["CorrectAnswer"].ToString().Trim(),
                        UserAnswer = ds.Tables[0].Rows[i]["UserAnswer"].ToString().Trim(),
                        BookmarkCount = ds.Tables[0].Rows[i]["BookmarkCount"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<Tuple<UserTestQuestionswithbookmark, int, string>> GetQuestionBookmarkReview(BookmarkRequest QuestionCode)
        {
            UserTestQuestionswithbookmark userMockTestQuestions = new UserTestQuestionswithbookmark();
            DataSet ds = new DataSet();
            List<Questionbargraph> questionList = new List<Questionbargraph>();

            ds = await objconnectedcaredal.GetQuestionBookmarkReview(QuestionCode);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("ResponseId") && ds.Tables[0].Rows[0]["ResponseId"].ToString() == "1")
            {
                var imageUrl = CoreHelper.AzureCloudUmedicoQuizQuestionImageDownloadURL();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    questionList.Add(new Questionbargraph
                    {
                        QuestionID = dr["QuestionCode"].ToString().TrimEnd(),
                        QuestionText = dr["QuestionName"].ToString().TrimEnd(),
                        Images = dr["questionimageurl"].ToString().TrimEnd(),
                        Options = ds != null && ds.Tables.Count > 1 && ds != null && ds.Tables[1].Rows.Count > 0 ? RepositoryHelper.GetOptionsBasedOnQuestionCode(dr["questioncode"].ToString(), ds.Tables[2]) : new List<string>(),
                        //Images = ds != null && ds.Tables.Count > 2 && ds != null && ds.Tables[2].Rows.Count > 0 ? RepositoryHelper.GetImagesBasedOnQuestionCode(imageUrl, dr["questioncode"].ToString(), ds.Tables[3]) : new List<string>(),
                        ActualAnswer = dr["Answer"].ToString().TrimEnd(),
                        UserAnswer = dr["useranswer"].ToString().TrimEnd(),
                        AnswerStatus = dr["AnswerStatus"].ToString().TrimEnd(),
                        BookmarkStatus = dr["BookmarkStatus"].ToString().TrimEnd(),
                        BookmarkCount = dr["BookmarkCount"].ToString().TrimEnd(),
                        OptionACount = !string.IsNullOrEmpty(dr["A"].ToString().TrimEnd()) ? dr["A"].ToString().TrimEnd() : "0",
                        OptionBCount = !string.IsNullOrEmpty(dr["B"].ToString().TrimEnd()) ? dr["B"].ToString().TrimEnd() : "0",
                        OptionCCount = !string.IsNullOrEmpty(dr["C"].ToString().TrimEnd()) ? dr["C"].ToString().TrimEnd() : "0",
                        OptionDCount = !string.IsNullOrEmpty(dr["D"].ToString().TrimEnd()) ? dr["D"].ToString().TrimEnd() : "0",
                        Explanation = dr["Explanation"].ToString().TrimEnd(),
                    });
                }
                userMockTestQuestions.questionList = questionList;
                userMockTestQuestions.TestStatus = UMedicoTestStatus.Initiated;
            }
            return new Tuple<UserTestQuestionswithbookmark, int, string>(userMockTestQuestions, Convert.ToInt32(ds.Tables[0].Rows[0]["ResponseId"].ToString()), ds.Tables[0].Rows[0]["ResponseText"].ToString());
        }
        public async Task<INCUSBaseResponseModel> SaveComments(CommentRequest commentRequest)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.SaveComments(commentRequest);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
                string GCMID = string.Empty;
                string Name = string.Empty;
                string Tournamentname = string.Empty;
                string Subcategoryid = string.Empty;
                string Phoneno = string.Empty;
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    GCMID = dr["GCMID"].ToString();
                    Name = dr["name"].ToString();
                    Tournamentname = dr["Tournamentname"].ToString();
                    Subcategoryid = dr["Subcategoryid"].ToString();
                    if (Phoneno == commentRequest.Phoneno)
                    {
                    }
                    else
                    {
                        NotifyAsync(GCMID, "INCUS Comments", "Hi " + Name + ", A new comment is posted on the Tournament " + Tournamentname, Subcategoryid, commentRequest.QuestionCode);
                    }
                }
            }
            return rd;

        }
        public async Task<List<GetCommentsList>> GetQuestionComments(string QuestionCode)
        {
            List<GetCommentsList> patientBankDetails = new List<GetCommentsList>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.GetQuestionComments(QuestionCode);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("name"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new GetCommentsList
                    {
                        CommentID = ds.Tables[0].Rows[i]["CommentID"].ToString().Trim(),
                        Name = ds.Tables[0].Rows[i]["name"].ToString().Trim(),
                        Profilepic = ds.Tables[0].Rows[i]["profilepic"].ToString().Trim(),
                        CommentTimestamp = ds.Tables[0].Rows[i]["commenttimestamp"].ToString().Trim(),
                        Comments = ds.Tables[0].Rows[i]["comments"].ToString().Trim(),
                        Commentimageurl = ds.Tables[0].Rows[i]["ImageURL"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<INCUSBaseResponseModel> SaveReportComments(CommentReport commentRequest)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.SaveReportComments(commentRequest);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<List<GetCommentslistofweb>> Incus_GetQuestionCommentsforReport()
        {
            List<GetCommentslistofweb> patientBankDetails = new List<GetCommentslistofweb>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.Incus_GetQuestionCommentsforReport();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("name"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new GetCommentslistofweb
                    {
                        CommentID = ds.Tables[0].Rows[i]["CommentID"].ToString().Trim(),
                        ReportID = ds.Tables[0].Rows[i]["ReportID"].ToString().Trim(),
                        Commenttext = ds.Tables[0].Rows[i]["Commenttext"].ToString().Trim(),
                        Commentimage = "https://incusblobstorage.blob.core.windows.net/imageresources/category/" + ds.Tables[0].Rows[i]["commentimage"].ToString().Trim(),
                        Commentatorname = ds.Tables[0].Rows[i]["name"].ToString().Trim(),
                        Commentatoremailid = ds.Tables[0].Rows[i]["emailid"].ToString().Trim(),
                        CommentattorMobileno = ds.Tables[0].Rows[i]["mobileno"].ToString().Trim(),
                        Mcqcontent = ds.Tables[0].Rows[i]["mcqcontent"].ToString().Trim(),
                        Mcqanswer = ds.Tables[0].Rows[i]["mcqanswer"].ToString().Trim(),
                        Game = ds.Tables[0].Rows[i]["game"].ToString().Trim(),
                        Gamecategory = ds.Tables[0].Rows[i]["gamecategory"].ToString().Trim(),
                        Pinnumber = ds.Tables[0].Rows[i]["pinnumber"].ToString().Trim(),
                        Reportername = ds.Tables[0].Rows[i]["reportername"].ToString().Trim(),
                        Reporteremailid = ds.Tables[0].Rows[i]["Reporteremailid"].ToString().Trim(),
                        Reportermobileno = ds.Tables[0].Rows[i]["reportermobileno"].ToString().Trim(),
                        Publishstatus = ds.Tables[0].Rows[i]["publishstatus"].ToString().Trim(),
                        Commenteddate = ds.Tables[0].Rows[i]["commenteddate"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<INCUSBaseResponseModel> Incus_RepublishComment(RepublishComment CommentID)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.Incus_RepublishComment(CommentID);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        #endregion
        #region Notifications
        public async void NotifyAsync(string to, string title, string body, string type, string Questioncode)
        {
            try
            {
                // Get the server key from FCM console
                var serverKey = string.Format("key={0}", "AAAAb-Xi2_4:APA91bELCbxdSHyXA3uF6dXlEIgajCyJC-jXeAgjEkW-0oGwfjTrF3Q9zwaODBf-9E1_bD5NKOY8l1lh6g0qAFMeYBHI1Ep2GPGksg5oQrLXQ0Gife6pzGbPd1hh__2Spft68-iOyShr");

                // Get the sender id from FCM console
                var senderId = string.Format("id={0}", "480598219774");
                type = "comments";
                title = title + "@" + Questioncode;
                var data = new
                {
                    to,
                    notification = new { title, body, type },
                    //  data = Questioncode
                };

                // Using Newtonsoft.Json
                var jsonBody = JsonConvert.SerializeObject(data);

                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send"))
                {
                    httpRequest.Headers.TryAddWithoutValidation("Authorization", serverKey);
                    httpRequest.Headers.TryAddWithoutValidation("Sender", senderId);
                    httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                    using (var httpClient = new HttpClient())
                    {
                        var result = await httpClient.SendAsync(httpRequest);

                        if (result.IsSuccessStatusCode)
                        {
                            //  return true;
                        }
                        else
                        {
                            // Use result.StatusCode to handle failure
                            // Your custom error handler here
                            //_logger.LogError($"Error sending notification. Status Code: {result.StatusCode}");
                        }
                    }
                }
            }
            catch (InvalidOperationException ex)
            { }
            //catch (Exception ex)
            //{
            //    // _logger.LogError($"Exception thrown in Notify Service: {ex}");
            //}
        }
        public async Task<Tuple<UserTestQuestionswithbookmark, int, string>> GetQuestionDetails(BookmarkRequest QuestionCode)
        {
            UserTestQuestionswithbookmark userMockTestQuestions = new UserTestQuestionswithbookmark();
            DataSet ds = new DataSet();
            List<Questionbargraph> questionList = new List<Questionbargraph>();

            ds = await objconnectedcaredal.GetQuestionDetails(QuestionCode);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("ResponseId") && ds.Tables[0].Rows[0]["ResponseId"].ToString() == "1")
            {
                var imageUrl = CoreHelper.AzureCloudUmedicoQuizQuestionImageDownloadURL();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    questionList.Add(new Questionbargraph
                    {
                        QuestionID = dr["QuestionCode"].ToString().TrimEnd(),
                        QuestionText = dr["QuestionName"].ToString().TrimEnd(),
                        Images = dr["questionimageurl"].ToString().TrimEnd(),
                        Options = ds != null && ds.Tables.Count > 1 && ds != null && ds.Tables[1].Rows.Count > 0 ? RepositoryHelper.GetOptionsBasedOnQuestionCode(dr["questioncode"].ToString(), ds.Tables[2]) : new List<string>(),
                        //Images = ds != null && ds.Tables.Count > 2 && ds != null && ds.Tables[2].Rows.Count > 0 ? RepositoryHelper.GetImagesBasedOnQuestionCode(imageUrl, dr["questioncode"].ToString(), ds.Tables[3]) : new List<string>(),
                        ActualAnswer = dr["Answer"].ToString().TrimEnd(),
                        UserAnswer = dr["useranswer"].ToString().TrimEnd(),
                        AnswerStatus = dr["AnswerStatus"].ToString().TrimEnd(),
                        BookmarkStatus = dr["BookmarkStatus"].ToString().TrimEnd(),
                        BookmarkCount = dr["BookmarkCount"].ToString().TrimEnd(),
                        OptionACount = !string.IsNullOrEmpty(dr["A"].ToString().TrimEnd()) ? dr["A"].ToString().TrimEnd() : "0",
                        OptionBCount = !string.IsNullOrEmpty(dr["B"].ToString().TrimEnd()) ? dr["B"].ToString().TrimEnd() : "0",
                        OptionCCount = !string.IsNullOrEmpty(dr["C"].ToString().TrimEnd()) ? dr["C"].ToString().TrimEnd() : "0",
                        OptionDCount = !string.IsNullOrEmpty(dr["D"].ToString().TrimEnd()) ? dr["D"].ToString().TrimEnd() : "0",
                        Explanation = dr["Explanation"].ToString().TrimEnd(),
                    });
                }
                userMockTestQuestions.questionList = questionList;
                userMockTestQuestions.TestStatus = UMedicoTestStatus.Initiated;
            }
            return new Tuple<UserTestQuestionswithbookmark, int, string>(userMockTestQuestions, Convert.ToInt32(ds.Tables[0].Rows[0]["ResponseId"].ToString()), ds.Tables[0].Rows[0]["ResponseText"].ToString());
        }
        #endregion
        #region Web APIs 
        public async Task<Tuple<List<QuestionforWeb>, string>> GetQuizQuestionsAll(QuestionSearchRequest Searchkey)
        {

            List<QuestionforWeb> userMockTestQuestions = new List<QuestionforWeb>();
            DataSet ds = new DataSet();
            string TotalCount = string.Empty;
            ds = await objconnectedcaredal.INCUS_GetQuizQuestionsAll(Searchkey);
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                TotalCount = dr["TotalCount"].ToString();
            }
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("CategoryName"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    userMockTestQuestions.Add(new QuestionforWeb
                    {
                        CategoryName = ds.Tables[0].Rows[i]["CategoryName"].ToString().Trim(),
                        TournamentName = ds.Tables[0].Rows[i]["subcategoryname"].ToString().Trim(),
                        TournamentPinnumber = ds.Tables[0].Rows[i]["Pinnumber"].ToString().Trim(),
                        QuestionID = ds.Tables[0].Rows[i]["Questioncode"].ToString().Trim(),
                        QuestionText = ds.Tables[0].Rows[i]["Questionname"].ToString().Trim(),
                        QuestionIMAGEURL = ds.Tables[0].Rows[i]["QuestionIMAGEURL"].ToString().Trim(),
                        QuestionAudioURL = ds.Tables[0].Rows[i]["QuestionAudioURL"].ToString().Trim(),
                        QuestionVideoURL = ds.Tables[0].Rows[i]["QuestionVideoURL"].ToString().Trim(),
                        ExplanationText = ds.Tables[0].Rows[i]["Explanation"].ToString().Trim(),
                        ExplanationImage1 = ds.Tables[0].Rows[i]["Explanation1"].ToString().Trim(),
                        ExplanationImage2 = ds.Tables[0].Rows[i]["Explanation2"].ToString().Trim(),
                        ExplanationImage3 = ds.Tables[0].Rows[i]["Explanation3"].ToString().Trim(),
                        ExplanationImage4 = ds.Tables[0].Rows[i]["Explanation4"].ToString().Trim(),
                        ExplanationImage5 = ds.Tables[0].Rows[i]["Explanation5"].ToString().Trim(),
                        ExplanationImage6 = ds.Tables[0].Rows[i]["Explanation6"].ToString().Trim(),
                        ExplanationImage7 = ds.Tables[0].Rows[i]["Explanation7"].ToString().Trim(),
                        ExplanationImage8 = ds.Tables[0].Rows[i]["Explanation8"].ToString().Trim(),
                        ExplanationImage9 = ds.Tables[0].Rows[i]["Explanation9"].ToString().Trim(),
                        ExplanationImage10 = ds.Tables[0].Rows[i]["Explanation10"].ToString().Trim(),
                        ActualAnswer = ds.Tables[0].Rows[i]["Answer"].ToString().Trim(),
                        OptionA = ds.Tables[0].Rows[i]["OptionA"].ToString().Trim(),
                        OptionB = ds.Tables[0].Rows[i]["OptionB"].ToString().Trim(),
                        OptionC = ds.Tables[0].Rows[i]["OptionC"].ToString().Trim(),
                        OptionD = ds.Tables[0].Rows[i]["OptionD"].ToString().Trim(),
                     //   OptionDCount = ds.Tables[0].Rows[i]["OptionD"].ToString().Trim(),
                        Subject = ds.Tables[0].Rows[i]["Subject"].ToString().Trim(),
                        Topic = ds.Tables[0].Rows[i]["Topic"].ToString().Trim(),
                    });
                }
            }
            return new Tuple<List<QuestionforWeb>, string>(userMockTestQuestions, TotalCount);
        }
        public async Task<INCUSBaseResponseModel> UpdateSubCategoryName(UpdateTournamentName verifyOtp)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.UpdateSubCategoryName(verifyOtp);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<INCUSBaseResponseModel> UpdateQuestionbyRecord(QuestionforWeb verifyOtp)
        {
            INCUSBaseResponseModel rd = new INCUSBaseResponseModel();

            DataSet ds = new DataSet();

            ds = await objconnectedcaredal.UpdateQuestionbyRecord(verifyOtp);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("responseId"))
            {
                if (ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd() == "1")
                    rd.Status = true;
                else
                    rd.Status = false;
                rd.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            return rd;

        }
        public async Task<List<GetCorrectwrongCountsReponse>> GetCorrectwrongCounts(GetCorrectwrongCountsRequest QuestionCode)
        {
            List<GetCorrectwrongCountsReponse> patientBankDetails = new List<GetCorrectwrongCountsReponse>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.GetCorrectwrongCounts(QuestionCode);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("questioncode"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new GetCorrectwrongCountsReponse
                    {
                        Questioncode = ds.Tables[0].Rows[i]["questioncode"].ToString().Trim(),
                        Questionname = ds.Tables[0].Rows[i]["questionname"].ToString().Trim(),
                        Categoryname = ds.Tables[0].Rows[i]["categoryname"].ToString().Trim(),
                        Subcategoryname = ds.Tables[0].Rows[i]["subCategoryname"].ToString().Trim(),
                        TotalResult = ds.Tables[0].Rows[i]["Result"].ToString().Trim(),
                        Totalanswered = ds.Tables[0].Rows[i]["TotalAnswered"].ToString().Trim(),
                        Bookmarkstatus = ds.Tables[0].Rows[i]["Bookmarkstatus"].ToString().Trim(),
                        ActualAnswer = ds.Tables[0].Rows[i]["actualanswer"].ToString().Trim(),
                        UserAnswer = ds.Tables[0].Rows[i]["answer"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        #endregion

        #region 7th Nov 2020
        public async Task<ResponseViewModel> Incus_SaveCourses(Courses SaveUserRegistration)
        {
            ResponseViewModel objnewres = new ResponseViewModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.Incus_SaveCourses(SaveUserRegistration);
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.State = true;
                }
                else
                {
                    objnewres.State = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            else
            {
                objnewres.State = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        public async Task<List<Courses>> Incus_GetCourses(string UserID)
        {
            List<Courses> patientBankDetails = new List<Courses>();
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.Incus_GetCourses(UserID);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("CourseID"))
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    patientBankDetails.Add(new Courses
                    {
                        CourseID = ds.Tables[0].Rows[i]["CourseID"].ToString().Trim(),
                        CourseName = ds.Tables[0].Rows[i]["CourseName"].ToString().Trim(),
                        CourseDescription = ds.Tables[0].Rows[i]["CourseDescription"].ToString().Trim(),
                        CourseImageURL = ds.Tables[0].Rows[i]["CourseImageURL"].ToString().Trim(),
                        CategoryID = ds.Tables[0].Rows[i]["CategoryID"].ToString().Trim(),
                        AboutAuthor = ds.Tables[0].Rows[i]["AboutAuthor"].ToString().Trim(),
                        SubscriptionDuration = ds.Tables[0].Rows[i]["SubscriptionDuration"].ToString().Trim(),
                        Price = ds.Tables[0].Rows[i]["Price"].ToString().Trim(),
                        TotalMCQs = ds.Tables[0].Rows[i]["TotalMCQs"].ToString().Trim(),
                        TotalTournaments = ds.Tables[0].Rows[i]["TotalTournaments"].ToString().Trim(),
                    });
                }
            }
            return patientBankDetails;
        }
        public async Task<ResponseViewModel> INCUS_SaveSubCategory(INCUSSubcategoryPL tournamentDetails)
        {
            ResponseViewModel objnewres = new ResponseViewModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.INCUS_SaveSubCategory(tournamentDetails);
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.State = true;
                }
                else
                {
                    objnewres.State = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            else
            {
                objnewres.State = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        public async Task<ResponseViewModel> INCUS_SaveQuestion(SaveQuestionPL tournamentDetails)
        {
            ResponseViewModel objnewres = new ResponseViewModel();
            string test = string.Empty;
            string Message = string.Empty;
            DataSet ds = new DataSet();
            ds = await objconnectedcaredal.INCUS_SaveQuestion(tournamentDetails);
            if (ds != null && ds.Tables.Count > 0)
            {
                test = ds.Tables[0].Rows[0]["responseId"].ToString().TrimEnd();
                if (test == "1")
                {
                    objnewres.State = true;
                }
                else
                {
                    objnewres.State = false;
                }
                objnewres.Message = ds.Tables[0].Rows[0]["responseText"].ToString().TrimEnd();
            }
            else
            {
                objnewres.State = false;
                objnewres.Message = "Already Exists";
            }
            return objnewres;
        }
        #endregion
    }
}

