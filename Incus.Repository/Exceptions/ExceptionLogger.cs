﻿using System;
using System.Threading.Tasks;

namespace Incus.Repository
{
    public class ExceptionLogger
    {
        private ILogger _logger;
        public ExceptionLogger(ILogger aLogger)
        {
            this._logger = aLogger;
        }
        public async Task<bool> LogException(string jsonRequest, Exception aException)
        {
            //string strMessage = GetUserReadableMessage(aException);
          return await this._logger.LogMessage(jsonRequest, aException);
        }

        private string GetUserReadableMessage(Exception aException)
        {

            string strMessage = string.Empty;
            return strMessage;

        }
    }
}
