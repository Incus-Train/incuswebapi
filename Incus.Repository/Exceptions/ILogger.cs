﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incus.Repository
{
    public interface ILogger
    {
        Task<bool> LogMessage(string jsonrequest, Exception ex);
    }
}
