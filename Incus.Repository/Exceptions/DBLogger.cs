﻿using Incus.Datalayer;
using Incus.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incus.Repository
{
    public class DBLogger : ILogger
    {
        public async Task<bool> LogMessage(string jsonrequest, Exception ex)
        {
            ExceptionLogEntity logentity = new ExceptionLogEntity();
            logentity.appName = ConfigurationManager.AppSettings["ExceptionLoggerAppName"] != null ? ConfigurationManager.AppSettings["ExceptionLoggerAppName"].ToString() : "Home Heal";
            logentity.errorMsg = ex != null ? ex.Message : string.Empty;
            logentity.innerMessage = ex != null ? Convert.ToString(ex.InnerException) : string.Empty;
            logentity.requestObj = jsonrequest;
            logentity.stackTrace = ex != null ? ex.StackTrace : string.Empty;
            ExceptionDal expLog = new ExceptionDal();
          return await expLog.LogMessage(logentity);
        }
    }
}
