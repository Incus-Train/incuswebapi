﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incus.Core.Common
{
    public enum FileUploadPurpose
    {
        DefaultFile,
        OrganizationLogo,
        OrganizationDocuments,
        ProfilePic,
        ProfileResume,
        ProfileVideoLink,
        JobDescriptionFile,
        MailAttachment,
        TempFile,
        PrivateFile,
        StaticFile
    }


}
