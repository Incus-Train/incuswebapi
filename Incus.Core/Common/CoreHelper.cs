﻿using Incus.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Net.Mail;
using System.Runtime.Remoting.Messaging;
using System.Reflection;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace Incus.Core.Common
{
    public static class CoreHelper
    {
        public static string AzureCloudUmedicoQuizQuestionImageDownloadURL()
        {
            return "https://umedicoliveappdeployed.azurewebsites.net/api/content/DownloadFile?directory=umediocquizimages&blobId=";
        }
        public static List<T> ToListof<T>(this DataTable dt)
        {
            List<T> targetList = new List<T>();
            try
            {
                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
                var columnNames = dt.Columns.Cast<DataColumn>()
                    .Select(c => c.ColumnName)
                    .ToList();
                var objectProperties = typeof(T).GetProperties(flags);
                targetList = dt.AsEnumerable().Select(dataRow =>
                {
                    var instanceOfT = Activator.CreateInstance<T>();

                    foreach (var properties in objectProperties.Where(properties => columnNames.Contains(properties.Name) && dataRow[properties.Name] != DBNull.Value))
                    {
                        properties.SetValue(instanceOfT, dataRow[properties.Name], null);
                    }
                    return instanceOfT;
                }).ToList();
            }
            catch (Exception ex)
            { }
            return targetList;
        }
        public async static Task BlockUserAtChat(string sessionCode)
        {
            try
            {
                var getAzureCloudHubUrlBasedOnDeploymentType = CoreHelper.GetAzureCloudChatHubUrlBasedOnDeploymentType();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(getAzureCloudHubUrlBasedOnDeploymentType);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders
                        .Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var httpResponse = await client.PostAsync("block/" + sessionCode, new StringContent(""));
                    httpResponse.EnsureSuccessStatusCode();
                }
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(sessionCode);
                //string ErrorSource = "Controller: Web API Utility/Helper.cs;Method: BlockUserAtChat;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
        }
        public static string GetAzureCloudChatHubUrlBasedOnDeploymentType()
        {
            //var DeploymentType = GetDeploymentType();
            // AzureCloudHubChatFor_LiveorProductionUrl
            return ConfigurationManager.AppSettings["AzureCloudHubChatFor_LiveorProductionUrl"] != null ? ConfigurationManager.AppSettings["AzureCloudHubChatFor_LiveorProductionUrl"] : "http://c2chubdev.cloudapp.net/api/chat/";
            //if (DeploymentType.ToLower().Contains("live") || DeploymentType.ToLower().Contains("prod"))
            //{
            //    return ConfigurationManager.AppSettings["AzureCloudHubChatFor_LiveorProductionUrl"] != null ? ConfigurationManager.AppSettings["AzureCloudHubChatFor_LiveorProductionUrl"] : "http://c2chubdev.cloudapp.net/api/chat/";
            //}
            //else
            //{
            //    return ConfigurationManager.AppSettings["AzureCloudHubChatFor_DevelopmentUrl"] != null ? ConfigurationManager.AppSettings["AzureCloudHubChatFor_DevelopmentUrl"] : "http://c2chub.cloudapp.net/api/chat/";
            //}
        }
        public static string IsDBEncriptAndDecriptRequired()
        {
            var DeploymentType = ConfigurationManager.AppSettings["IsDBEncriptAndDecriptRequired"] != null ? ConfigurationManager.AppSettings["IsDBEncriptAndDecriptRequired"] : "false";
            return DeploymentType;
        }
        public static string AzureCloudPatientProfilePicURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudPatientProfilePicURL"] != null ? ConfigurationManager.AppSettings["AzureCloudPatientProfilePicURL"] : "http://hub.clinicopedia.org/api/content/patientProfile/";
        }
        public static string AzureCloudConnectedCareSrikaraImagesURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudConnectedCareSrikaraImagesURL"] != null ? ConfigurationManager.AppSettings["AzureCloudConnectedCareSrikaraImagesURL"] : "http://hub.clinicopedia.org/api/content/sriKara/";
        }
        public static string AzureCloudGroupImagesPicURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudGroupImagesPicURL"] != null ? ConfigurationManager.AppSettings["AzureCloudPatientProfilePicURL"] : "http://hub.clinicopedia.org/api/content/groupchatimages/";
        }
        public static string AzureCloudCategoryPicURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudCategoryPic"] != null ? ConfigurationManager.AppSettings["AzureCloudCategoryPic"] : "https://click2clinicstorage.blob.core.windows.net/umedico-public/subjects/";
        }
        public static string AzureCloudHealthManagerPatientIdProofPicURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHealthManagerPatientIdProofPicURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHealthManagerPatientIdProofPicURL"] : "http://hub.clinicopedia.org/api/content/HealthCallPatientIdProofImages/";
        }
        public static string AzureCloudHIMSImagesURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHIMSImagesURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHIMSImagesURL"] : "http://hub.clinicopedia.org/api/content/himsdocuments/";
        }
        public static string AzureCloudHealthCallWhatsOnMindImagesURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHealthCallWhatsOnMindImagesURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHealthCallWhatsOnMindImagesURL"] : "http://chathub.clinicopedia.org/api/content/healthcallwhatsonmindimages/";
        }
        public static string AzureCloudHealthCallPostMyCaseImagesURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHealthCallPostMyCaseImagesURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHealthCallPostMyCaseImagesURL"] : "http://chathub.clinicopedia.org/api/content/healthcallpostmycaseimages/";
        }
        public static string AzureCloudHealthCallPostOffersImageURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHealthCallPostOffersImageURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHealthCallPostOffersImageURL"] : "http://hub.clinicopedia.org/api/content/healthcallofferimages/";
        }

        public static string AzureCloudPharmaProfilePicDownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudPharmacyImagesPicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudPharmacyImagesPicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/pharmacyimages/";
        }
        public static string AzureCloudArticlePicDownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudArticleImagesPicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudArticleImagesPicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/articleimages/";
        }
        public static string AzureCloudPollsPicDownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudPollsImagesPicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudPollsImagesPicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/pollimages/";
        }
        public static string AzureCloudHealthNewsPicDownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHealthNewsImagesPicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHealthNewsImagesPicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/healthnews/";
        }
        public static string AzureCloudCoursesPicDownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudCoursesImagesPicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudCoursesImagesPicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/courses/";
        }
        public static string AzureCloudConnectedCarePatientImagesURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudConnectedCarePatientImagesURL"] != null ? ConfigurationManager.AppSettings["AzureCloudConnectedCarePatientImagesURL"] : "http://hub.clinicopedia.org/api/content/connectedcare/";
        }

        public static string AzureCloudDoctorImagesDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudDoctorProfilePicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudDoctorProfilePicDownloadURL"] : "http://hub.clinicopedia.org/api/content/doctor/";
        }
        public static string AzureCloudDepartmentPicdownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudDepartmentPicdownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudDepartmentPicdownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/healthcalldepartmentimages/";
        }

        public static string AzureCloudNurseImagesDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudNurseProfilePicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudNurseProfilePicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/healthcallnurseimages/";
        }
        public static string AzureCloudHealthcallDiagnosticProfilePicDownloadURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudDiagnosticProfilePicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudDiagnosticProfilePicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/healthcalldiagnosticimages/";
        }
        public static string AzureCloudHCPVideoPicDownloadFrom_clinicopedia_public_URL()
        {
            return ConfigurationManager.AppSettings["AzureCloudHCPVideoPicDownloadURL"] != null ? ConfigurationManager.AppSettings["AzureCloudHCPVideoPicDownloadURL"] : "https://click2clinicstorage.blob.core.windows.net/clinicopedia-public/healthcallhcpvideos/";
        }

        public static bool CheckIsStringIsEmptyOrNull(string strValue)
        {
            if (!string.IsNullOrEmpty(strValue))
                return true;
            else
                return false;
        }

        public static string GetDeploymentType()
        {
            var DeploymentType = ConfigurationManager.AppSettings["DeploymentType"] != null ? ConfigurationManager.AppSettings["DeploymentType"] : "live";
            return DeploymentType;
        }

        public static string CheckCityName(string city)
        {
            if (string.IsNullOrEmpty(city.Trim()))
                return "Not Mentioned";
            else
                return city.Trim();
        }
        public static string AzureCloudHospitalBranchImagePath()
        {
            return ConfigurationManager.AppSettings["AzureCloudHospitalBranchImagePath"] != null ? ConfigurationManager.AppSettings["AzureCloudHospitalBranchImagePath"] : "https://click2clinicstorage.blob.core.windows.net/click2clinic/hospitalbranchimages/";
        }
        public static string AzureCloudHospitalDepartmentImagePath()
        {
            return ConfigurationManager.AppSettings["AzureCloudHospitalDepartmentImagePath"] != null ? ConfigurationManager.AppSettings["AzureCloudHospitalDepartmentImagePath"] : "https://click2clinicstorage.blob.core.windows.net/click2clinic/hospitaldepartmentimages/";
        }
        public static string AzureCloudFamilyPackageMembersDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudFamilyPackageMembersDownloadingURL"] != null ? ConfigurationManager.AppSettings["AzureCloudFamilyPackageMembersDownloadingURL"] : "http://hub.clinicopedia.org/api/content/familypackagemembers/";
        }
        public static string AzureCloudFamilyPackageScheduledDoctorDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudFamilyPackageScheduledDoctorDownloadingURL"] != null ? ConfigurationManager.AppSettings["AzureCloudFamilyPackageScheduledDoctorDownloadingURL"] : "http://hub.clinicopedia.org/api/content/familypackagedoctor/";
        }
        public static string AzureCloudPackageImageDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudPackageImageDownloadingURL"] != null ? ConfigurationManager.AppSettings["AzureCloudPackageImageDownloadingURL"] : "http://hub.clinicopedia.org/api/content/packageimages/";
        }
        public static string AzureCloudFamilyDoctorPrescriptionsDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudFamilyDoctorPrescriptionsDownloadingURL"] != null ? ConfigurationManager.AppSettings["AzureCloudFamilyDoctorPrescriptionsDownloadingURL"] : "http://hub.clinicopedia.org/api/content/FamilyDoctorPrescriptions/";
        }

        public static string AzureCloudHospitalImagePath()
        {
            return ConfigurationManager.AppSettings["AzureCloudHospitalImagePath"] != null ? ConfigurationManager.AppSettings["AzureCloudHospitalImagePath"] : "https://click2clinicstorage.blob.core.windows.net/click2clinic/healthcallhospitalimages/";
        }
        public static string AzureCloudConnectHospitalImagePath()
        {
            return ConfigurationManager.AppSettings["AzureCloudConnectHospitalImagePath"] != null ? ConfigurationManager.AppSettings["AzureCloudConnectHospitalImagePath"] : "http://hub.clinicopedia.org/api/content/connectedcare/";
        }

        public static string GetApplicationRefNo(string second)
        {
            try
            {
                string ApplicationRefNo = second;
                long third = (Convert.ToInt64(second) + 1);
                if (third < 10)
                    ApplicationRefNo = "000000000" + third;
                else if (third < 100 && third > 9)
                    ApplicationRefNo = "00000000" + third;
                else if (third < 1000 && third > 99)
                    ApplicationRefNo = "0000000" + third;
                else if (third < 10000 && third > 999)
                    ApplicationRefNo = "000000" + third;
                else if (third < 100000 && third > 9999)
                    ApplicationRefNo = "00000" + third;
                else if (third < 1000000 && third > 99999)
                    ApplicationRefNo = "0000" + third;
                else if (third < 10000000 && third > 999999)
                    ApplicationRefNo = "000" + third;
                else if (third < 100000000 && third > 9999999)
                    ApplicationRefNo = "00" + third;
                else if (third < 1000000000 && third > 99999999)
                    ApplicationRefNo = "0" + third;
                else if (third > 999999999)
                    ApplicationRefNo = third.ToString();

                return ApplicationRefNo;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string GetSessionIdWithAppendingTypeOfHCP(string typeOfHCP)
        {
            var r = new Random();
            var sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + typeOfHCP;
            return sessionId;
        }
        public static string GeTypeOfHCPFromSessionId(string sessionId)
        {
            string typeOfHcpName = string.Empty;
            if (!string.IsNullOrEmpty(sessionId) && sessionId.Length > 0 && sessionId.Contains('_'))
                typeOfHcpName = sessionId.Substring(sessionId.LastIndexOf('_') + 1).ToUpper();
            return typeOfHcpName;
        }
        public static string GetSessionId()
        {
            var r = new Random();
            var sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString();
            return sessionId;
        }
        public static string FrameLastVisitedOnString(string lastVisitedOnDays, string lastVisitedOnHours, string lastVisitedOnMin)
        {
            string lastVisitedString = string.Empty;
            StringBuilder lastVisitedOnString = new StringBuilder();
            try
            {
                UInt32 intlastVisitedOnDays = 0;
                UInt32 intlastVisitedOnHrs = 0;
                UInt32 intlastVisitedOnMins = 0;

                if (!string.IsNullOrEmpty(lastVisitedOnDays) && lastVisitedOnDays.ToUpper() != "NULL" && lastVisitedOnDays != "0")
                {
                    intlastVisitedOnDays = Convert.ToUInt32(lastVisitedOnDays);
                }

                if (!string.IsNullOrEmpty(lastVisitedOnHours) && lastVisitedOnHours.ToUpper() != "NULL" && lastVisitedOnHours != "0")
                {
                    intlastVisitedOnHrs = Convert.ToUInt32(lastVisitedOnHours);
                }

                if (!string.IsNullOrEmpty(lastVisitedOnMin) && lastVisitedOnMin.ToUpper() != "NULL" && lastVisitedOnMin != "0")
                {
                    intlastVisitedOnMins = Convert.ToUInt32(lastVisitedOnMin);
                }

                if (intlastVisitedOnDays == 0 && intlastVisitedOnHrs == 0 && intlastVisitedOnMins == 0)
                {
                    lastVisitedString = "Online";
                }
                else
                {
                    if (intlastVisitedOnMins > 60)
                    {

                        intlastVisitedOnHrs = intlastVisitedOnHrs + Convert.ToUInt32(lastVisitedOnMin) / 60;
                        intlastVisitedOnMins = Convert.ToUInt32(lastVisitedOnMin) % 60;
                    }

                    if (intlastVisitedOnHrs > 24)
                    {
                        intlastVisitedOnDays = intlastVisitedOnDays + Convert.ToUInt32(lastVisitedOnHours) / 24;
                        intlastVisitedOnHrs = Convert.ToUInt32(lastVisitedOnHours) % 24;
                    }

                    if (intlastVisitedOnDays == 1)
                        lastVisitedOnString.Append(intlastVisitedOnDays.ToString() + " Day ");
                    else if (intlastVisitedOnDays > 1)
                        lastVisitedOnString.Append(intlastVisitedOnDays.ToString() + " Days ");

                    if (intlastVisitedOnHrs > 0)
                        lastVisitedOnString.Append(intlastVisitedOnHrs + " hr ");

                    if (intlastVisitedOnMins > 0)
                        lastVisitedOnString.Append(intlastVisitedOnMins + " min");

                    if (lastVisitedOnString.ToString().Length > 0)
                        lastVisitedString = "Last online at " + lastVisitedOnString.ToString() + " ago";
                }
            }
            catch (Exception ex)
            {

            }

            return lastVisitedString;
        }
        public static string GetHCPProfilePicBaseurl(string typeofHCP)
        {
            var hCPimageUrl = string.Empty;

            if (typeofHCP.ToLower().Contains("doctor"))
                hCPimageUrl = CoreHelper.AzureCloudDoctorImagesDownloadingURL();
            else if (typeofHCP.ToLower().Contains("nurse"))
                hCPimageUrl = CoreHelper.AzureCloudNurseImagesDownloadingURL();

            return hCPimageUrl;
        }
        public static string Serialize(object dataToSerialize)
        {
            if (dataToSerialize == null) return null;

            using (StringWriter stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(dataToSerialize.GetType());
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                serializer.Serialize(stringwriter, dataToSerialize, ns);
                return stringwriter.ToString();
            }
        }
        public static string Encrypt(string raw)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                ICryptoTransform e = GetCryptoTransform(csp, true);
                byte[] inputBuffer = Encoding.UTF8.GetBytes(raw);
                byte[] output = e.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);

                string encrypted = Convert.ToBase64String(output);

                return encrypted;
            }
        }
        public static string Decrypt(string encrypted)
        {
            using (var csp = new AesCryptoServiceProvider())
            {
                var d = GetCryptoTransform(csp, false);
                byte[] output = Convert.FromBase64String(encrypted);
                byte[] decryptedOutput = d.TransformFinalBlock(output, 0, output.Length);

                string decypted = Encoding.UTF8.GetString(decryptedOutput);
                return decypted;
            }
        }
        private static ICryptoTransform GetCryptoTransform(AesCryptoServiceProvider csp, bool encrypting)
        {
            csp.Mode = CipherMode.CBC;
            csp.Padding = PaddingMode.PKCS7;
            var passWord = ConfigurationManager.AppSettings["AesCryptPassword"] != null ? ConfigurationManager.AppSettings["AesCryptPassword"].ToString() : "Pass@word1";
            var salt = ConfigurationManager.AppSettings["AesCryptSalt"] != null ? ConfigurationManager.AppSettings["AesCryptSalt"].ToString() : "S@1tS@lt";

            //a random Init. Vector. just for testing
            String iv = "e675f725e675f725";

            var spec = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(passWord), Encoding.UTF8.GetBytes(salt), 65536);
            byte[] key = spec.GetBytes(16);


            csp.IV = Encoding.UTF8.GetBytes(iv);
            csp.Key = key;
            if (encrypting)
            {
                return csp.CreateEncryptor();
            }
            return csp.CreateDecryptor();
        }
        public static async Task<string> SendSMSAsync(string mobileNo, string SMScontent)
        {
            string smslink;
            string smspage;
            string username;
            string password;
            string senderid;
            string query;
            string sr = null;
            try
            {
                return await Task.Run(() =>
                {

                    //http://onex-ultimo.in/api/pushsms?user=500566&authkey=92cHRlXTqcchY&sender=CLKNIC&mobile=9800000000&text=testing&rpt=1
                    smslink = System.Configuration.ConfigurationManager.AppSettings["SMSURL"];
                    smspage = System.Configuration.ConfigurationManager.AppSettings["smspage"];
                    username = System.Configuration.ConfigurationManager.AppSettings["uname"];
                    password = System.Configuration.ConfigurationManager.AppSettings["pass"];
                    senderid = System.Configuration.ConfigurationManager.AppSettings["senderid"];
                    //http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=click2clinic&passwd=l3tm3in@123&
                    //mobilenumber=919611863337&message=Hi%20this%20is%20kishore&sid=SMSCountry&mtype=N&DR=Y

                    //http://182.18.160.225/index.php/api/bulk-sms?username=click2clinic&password=12345678&from=CCLINC&to=919611863337&message=Web%20Test&sms_type=2
                    //   http://onex-ultimo.in/api/pushsms?user=500566&authkey=92cHRlXTqcchY&sender=CLKNIC&mobile=9800000000&text=testing&rpt=1
                    //query = "username=" + username + "&password=" + password + "&from=" + senderid + "&to=" + mobileNo + "&message=" + SMScontent + "&sms_type=2";
                    //query = "username=" + username + "&password=" + password + "&from=" + senderid + "&to=" + mobileNo + "&msg=" + SMScontent + "&type=1&dnd_check=0";
                    query = "apikey=f5ede346-8581-45ca-8778-3b595eb659e2&clientId=2e70a678-6bc2-4f4f-8adc-efadac102703&msisdn=" + mobileNo + "&sid=CLICKC&msg=" + SMScontent + "&fl=0&gwid=2";
                    byte[] b = null;
                    UriBuilder U = null;
                    WebClient w = null;

                    string sURL;
                    sURL = "http://onex-ultimo.in/api/pushsms?user=500566&authkey=92cHRlXTqcchY&sender=CUCHAT&mobile=" + mobileNo + "&text=" + SMScontent + "&rpt=1";
                    try
                    {
                        //b = new byte[1024];
                        //U = new UriBuilder("http", smslink);
                        //U.Path = smspage;
                        //U.Query = query;
                        //Uri uri = U.Uri;
                        //w = new WebClient();
                        ////// "Tranjectionid=" + uid + "&void=false&DeviceId=" + deviceid;
                        //string asd = Convert.ToString(uri);
                        //Stream s = w.OpenRead(uri.ToString());
                        //s.Read(b, 0, b.Length);

                        //s.Close();
                        //sr = System.Text.ASCIIEncoding.ASCII.GetString(b);
                        using (WebClient client = new WebClient())
                        {

                            string s = client.DownloadString(sURL);

                            var responseObject = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(s);
                            int n = responseObject.Status;
                            if (n == 3)
                            {
                                //MessageBox.Show("Something went wrong with your credentials", "My Demo App");

                            }
                            else
                            {
                                // MessageBox.Show("Message Send Successfully", "My Demo App");
                            }

                        }
                    }
                    catch (Exception exp)
                    {
                        return exp.ToString();
                        //     WriteErrorLogMessage("Web URL Exception is PostToURL" + exp.ToString());
                    }
                    finally
                    {
                        w = null;
                        b = null;
                        U = null;
                    }

                    int start = sr.IndexOfAny(new char[] { '\r', '\n', '\0' }, 0, sr.Length);
                    if (start != -1)
                        sr = sr.Substring(0, start);
                    return sr;
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(null);
                //string ErrorSource = " Class Name: CliniOpedia Web API/Helper.cs ;Method: SendSMSAsync;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sr;
        }
        public static async Task<string> SendNotification(string userValue,
                                                          string Messageoutput,
                                                          string gcmId,
                                                          int notificationType,
                                                          string proCode,
                                                          string doctorId
            )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    ProCode = proCode,
                                    DoctorId = doctorId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    ProCode = proCode,
                                    DoctorId = doctorId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendNotification(string userValue,
                                                          string Messageoutput,
                                                          string gcmId,
                                                          int notificationType,
                                                          string patientId
            )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    DoctorId = patientId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    PatientId = patientId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendNotification(string userValue,
                                                         string Messageoutput,
                                                         string gcmId,
                                                         int notificationType,
                                                         string proCode,
                                                         string doctorId,
                                                         string typeOfHCP
           )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    ProCode = proCode,
                                    DoctorId = doctorId,
                                    TypeOfHcp = typeOfHCP,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    ProCode = proCode,
                                    DoctorId = doctorId,
                                    TypeOfHcp = typeOfHCP,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendChatCreateNotification(string userValue,
                                                         string Messageoutput,
                                                         string gcmId,
                                                         int notificationType,
                                                         string SessionCode,
                                                         string GroupName,
                                                         string GroupID,
                                                         string image,
                                                         string createdByMobileNum,
                                                         string categoryCode
           )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    CategoryCode = categoryCode,
                                    SessionCode = SessionCode,
                                    GroupID = GroupID,
                                    GroupName = GroupName,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    imageURL = image,
                                    Creatdby = createdByMobileNum,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    CategoryCode = categoryCode,
                                    SessionCode = SessionCode,
                                    GroupID = GroupID,
                                    GroupName = GroupName,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    imageURL = image,
                                    Creatdby = createdByMobileNum,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //C2C.SrikaraHospital.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new C2C.SrikaraHospital.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }


        public static string GetSessionId_ForFamilyDoctor(string typeofconsultation)
        {
            var r = new Random();
            var sessionId = string.Empty;
            if (typeofconsultation.ToUpper() == "VIDEOCALL")
            {
                sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "FM_VD";
            }
            else if (typeofconsultation.ToUpper() == "ONCALLHOMEVISIT")
            {
                sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "FM_OCH";
            }
            else if (typeofconsultation.ToUpper() == "NETWORKCALL")
            {
                sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "FM_NW";
            }
            return sessionId;
        }
        public static string GetSessionId_ForDiabetic(string typeofconsultation)
        {
            var r = new Random();
            var sessionId = string.Empty;
            if (typeofconsultation.ToUpper() == "DIABETIC")
            {
                sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "GM_DB";
            }
            return sessionId;
        }
        public static string GetSessionId_ForHealthManager()
        {
            var r = new Random();
            var sessionId = string.Empty;
            sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "HM_DB";
            return sessionId;
        }
        public static string GetSessionId_ForDiabeticChatUser()
        {
            var r = new Random();
            var sessionId = string.Empty;
            sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "HM_DC";
            return sessionId;
        }
        public static string GetSessionId_ForHealthManager_DoctorEncounter()
        {
            var r = new Random();
            var sessionId = string.Empty;
            //if (typeofconsultation.ToUpper() == "HEALTHMANAGER")
            //{
            sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "HM_EN";
            //}
            return sessionId;
        }
        public static string GetSessionId_ForHealthManager_PatientProShare()
        {
            var r = new Random();
            var sessionId = string.Empty;
            sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "HM_DPS";
            return sessionId;
        }
        public static string GetSessionId_ForHealthManager_ACOShareChat()
        {
            var r = new Random();
            var sessionId = string.Empty;
            sessionId = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.ToUniversalTime().Ticks) + r.Next(1000, 9999).ToString() + "_" + "HM_ASC";
            return sessionId;
        }
        public static string AzureCloudProtocalXMLFilesURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudProtocalXMLFilesURL"] != null ? ConfigurationManager.AppSettings["AzureCloudProtocalXMLFilesURL"] : "http://hub.clinicopedia.org/api/content/ProtocalXMLFiles/";
        }
        public static string AzureCloudMealthManagerSqLiteFileUrlDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudMealthManagerSqLiteFileUrlDownloadingURL"] != null ? ConfigurationManager.AppSettings["AzureCloudMealthManagerSqLiteFileUrlDownloadingURL"] : "http://hub.clinicopedia.org/api/content/SqLiteFiles/HealthManager.sqlite";
        }
        public static string AzureCloudMealthManagerDBFileUrlDownloadingURL()
        {
            return ConfigurationManager.AppSettings["AzureCloudMealthManagerDBFileUrlDownloadingURL"] != null ? ConfigurationManager.AppSettings["AzureCloudMealthManagerDBFileUrlDownloadingURL"] : "http://hub.clinicopedia.org/api/content/SqLiteFiles/HealthManager.db";
        }
        public static async Task<string> SendPatientBookAppointmentStatusNotification(string userValue, string Messageoutput, string gcmId, int notificationType, string DoctorUserID, string UserType, string PatientEncounterCode)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    DoctorUserID = DoctorUserID,
                                    UserType = UserType,
                                    PatientEncounterCode = PatientEncounterCode,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    DoctorUserID = DoctorUserID,
                                    UserType = UserType,
                                    PatientEncounterCode = PatientEncounterCode,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }

        public static async Task<string> SendPatientBookAppointmentNotification(string userValue,
                                                        string Messageoutput,
                                                        string gcmId,
                                                        int notificationType,
                                                        string SelectedPHCID,
                                                        string ClerkUserid,
                                                        string SessionCode,
                                                        string ClerkName
          )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    SelectedPHCID = SelectedPHCID,
                                    ClerkUserid = ClerkUserid,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    SelectedPHCID = SelectedPHCID,
                                    ClerkUserid = ClerkUserid,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }

        public static async Task<string> SendPaymentDoneNotification(string userValue, string Messageoutput, string gcmId, int notificationType, string DoctorID, string UserType, string PatientEncounterCode)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    DoctorID = DoctorID,
                                    UserType = UserType,
                                    PatientEncounterCode = PatientEncounterCode,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    DoctorID = DoctorID,
                                    UserType = UserType,
                                    PatientEncounterCode = PatientEncounterCode,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }



        public static string GetRandomNumberCodeUp6Digits()
        {
            var r = new Random();
            var sessionId = r.Next(100000, 999999).ToString();
            return sessionId;
        }

        public static async Task<string> PharmacySendNotification(string userValue,
                                                        string Messageoutput,
                                                        string gcmId,
                                                        int notificationType,
                                                        string patientId
          )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    DoctorId = patientId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    PatientId = patientId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }


        public static async Task<string> SendNotificationToSupplier(string userValue,
                                                         string Messageoutput,
                                                         string gcmId,
                                                         int notificationType,
                                                         string returnTransactionCode,
                                                         string supplierName,
                                                         string supplierCode
           )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    ReturnTransactionCode = returnTransactionCode,
                                    SupplierName = supplierName,
                                    SupplierCode = supplierCode,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    ReturnTransactionCode = returnTransactionCode,
                                    SupplierName = supplierName,
                                    SupplierCode = supplierCode,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }




        public static async Task<string> SendNotificationForPayment(string userValue,
                                                       string Messageoutput,
                                                       string gcmId,
                                                       int notificationType,
                                                       string userID,
                                                       string doctorId,
                                                       string bookingID,
                                                       string typeOfRequest,
                                                       string address,
                                                       string paypalAmount,
                                                       string payUAmount
         )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    UserID = userID,
                                    DoctorId = doctorId,
                                    BookingAppID = bookingID,
                                    TypeOfRequest = typeOfRequest,
                                    Address = address,
                                    PayUAmount = payUAmount,
                                    PaypalAmount = paypalAmount,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    UserID = userID,
                                    DoctorId = doctorId,
                                    BookingAppID = bookingID,
                                    TypeOfRequest = typeOfRequest,
                                    Address = address,
                                    PayUAmount = payUAmount,
                                    PaypalAmount = paypalAmount,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }

        public static async Task<string> SendInCaseOfSavePatientPackageNotification(string userValue,
                                                                                string Messageoutput,
                                                                                string gcmId,
                                                                                int notificationType, string status)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    Status = status,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    Status = status,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
            }
            return sResponseFromServer;
        }

        public static async Task<string> SendLeadPaymentConfirmationNotification(string userValue,
                                                                              string Messageoutput,
                                                                              string gcmId,
                                                                              int notificationType,
                                                                              string doctorID,
                                                                              string leadID)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    DoctorId = doctorID,
                                    LeadID = leadID,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    DoctorId = doctorID,
                                    LeadID = leadID,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
            }
            return sResponseFromServer;
        }

        public static async Task<string> SendLeadPaymentConfirmationNotificationnew(string userValue,
                                                                              string Messageoutput,
                                                                              string gcmId,
                                                                              int notificationType,
                                                                              string doctorID,
                                                                              string leadID,
                                                                              string SessionID)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    DoctorId = doctorID,
                                    LeadID = leadID,
                                    SessionID = SessionID,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    DoctorId = doctorID,
                                    LeadID = leadID,
                                    SessionID = SessionID,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
            }
            return sResponseFromServer;
        }
        public static bool IsSMSNeedToSendToC2C_CTOInCaseOfNewUmedicoLiveShowAdded()
        {
            var value = ConfigurationManager.AppSettings["IsSMSNeedToSendToC2C_CTOInCaseOfNewUmedicoLiveShowAdded"] != null ? ConfigurationManager.AppSettings["IsSMSNeedToSendToC2C_CTOInCaseOfNewUmedicoLiveShowAdded"] : "false";
            return value.ToLower().Contains("true") ? true : false;
        }



        public class Base
        {
            public delegate bool SendEmailDelegate(string toEmail, string toName);

            public string FromEmail { get; set; }

            public string FromName { get; set; }

            public string MessageBody { get; set; }

            public int Port { get; set; }

            public NetworkCredential SmtpCredentials { get; set; }

            public string SmtpServer { get; set; }

            public string Subject { get; set; }

            public void GetResultsOnCallback(IAsyncResult ar)
            {
                SendEmailDelegate del = (SendEmailDelegate)((AsyncResult)ar).AsyncDelegate;
                try
                {
                    del.EndInvoke(ar);
                }
                catch (Exception)
                {
                    // bool result = false;
                }
            }

            public bool SendEmail(string toEmail, string toName)
            {
                try
                {
                    MailMessage Message = new MailMessage();
                    Message.IsBodyHtml = true;
                    Message.To.Add(new MailAddress(toEmail, toName));

                    //if(!string.IsNullOrEmpty(ccEmail) && !string.IsNullOrEmpty(ccName))
                    //Message.CC.Add(new MailAddress(toEmail, toName));

                    Message.From = (new MailAddress(this.FromEmail, this.FromName));
                    Message.Subject = this.Subject;
                    Message.Body = this.MessageBody;

                    SmtpClient sc = new SmtpClient();
                    sc.Port = this.Port;
                    sc.Host = this.SmtpServer;
                    sc.EnableSsl = true;
                    sc.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUsername"], ConfigurationManager.AppSettings["SMTPPassword"]);

                    sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                    sc.Send(Message);
                }
                catch (Exception ex)
                {
                    return false;
                }
                return true;
            }
            public bool SendEmail(string[] emails)
            {
                try
                {
                    MailMessage Message = new MailMessage();
                    Message.IsBodyHtml = true;
                    foreach (var mailId in emails)
                        Message.To.Add(new MailAddress(mailId, mailId));

                    //if(!string.IsNullOrEmpty(ccEmail) && !string.IsNullOrEmpty(ccName))
                    //Message.CC.Add(new MailAddress(toEmail, toName));

                    Message.From = (new MailAddress(this.FromEmail, this.FromName));
                    Message.Subject = this.Subject;
                    Message.Body = this.MessageBody;

                    SmtpClient sc = new SmtpClient();
                    sc.Port = this.Port;
                    sc.Host = this.SmtpServer;
                    sc.EnableSsl = true;
                    sc.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUsername"], ConfigurationManager.AppSettings["SMTPPassword"]);

                    sc.DeliveryMethod = SmtpDeliveryMethod.Network;
                    sc.Send(Message);
                }
                catch (Exception ex)
                {
                    return false;
                }
                return true;
            }

            public bool SendEmailAsync(string toEmail, string toName)
            {
                try
                {
                    SendEmailDelegate dc = new SendEmailDelegate(this.SendEmail);
                    AsyncCallback cb = new AsyncCallback(this.GetResultsOnCallback);
                    IAsyncResult ar = dc.BeginInvoke(toEmail, toName, cb, null);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static async Task<string> SendLeadDisputeStatusNotification(string userValue,
                                                                            string Messageoutput,
                                                                            string gcmId,
                                                                            int notificationType,
                                                                            string doctorID,
                                                                            string leadID,
                                                                            string disputeStatus)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    DoctorID = doctorID,
                                    LeadID = leadID,
                                    DisputeStatus = disputeStatus,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    DoctorID = doctorID,
                                    LeadID = leadID,
                                    DisputeStatus = disputeStatus,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
            }
            return sResponseFromServer;
        }


        public static async Task<string> LeadCompltedSendNotification(string userValue,
                                                      string Messageoutput,
                                                      string gcmId,
                                                      int notificationType,
                                                      string leadId,
                                                      string typeOfHcp,
                                                      string userType,
                                                      string patientMobile
        )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    LeadId = leadId,
                                    TypeOfHcp = typeOfHcp,
                                    UserType = userType,
                                    MobileNumber = patientMobile,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                //to = "dve0DC93JRY:APA91bGM2I6vnLb88thSyMCcAv4uegqb8Pc8fxLAfHn1bkKqYVymbxlLikgIZmJ9rURuD7t-W-f82SpAM_wf1yOQddAhWa0-ilFm-J1Ur966BaUre2-zd14CREcgPdK8WU34pnmtHKh-",
                                notification = new
                                {
                                    LeadId = leadId,
                                    TypeOfHcp = typeOfHcp,
                                    UserType = userType,
                                    MobileNumber = patientMobile,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Learnopedia.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Learnopedia.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }


        public static async Task<string> SendNotificationBookAppointment(string userValue,
                                                      string Messageoutput,
                                                      string gcmId,
                                                      int notificationType,
                                                      string SelectedPHCID,
                                                      string DoctorID,
                                                      string TypeOfHCP,
                                                       string proCode


        )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    SelectedPHCID = SelectedPHCID,
                                    ProCode = proCode,
                                    DoctorId = DoctorID,
                                    TypeOfHcp = TypeOfHCP,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    SelectedPHCID = SelectedPHCID,
                                    ProCode = proCode,
                                    DoctorId = DoctorID,
                                    TypeOfHcp = TypeOfHCP,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendNotificationWhenSaveChatHCPPaymentStatus(string userValue,
                                                                                                 string Messageoutput,
                                                                                                 string gcmId,
                                                                                                 int notificationType,
                                                                                                 string adminUserID)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    AdminUserID = adminUserID,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    AdminUserID = adminUserID,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendNotificationForChatSendNotification(string userValue,
                                                                                                 string Messageoutput,
                                                                                                 string gcmId,
                                                                                                 int notificationType,
                                                                                                 string adminUserID, string sourseUserName = "")
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    AdminUserID = adminUserID,
                                    NotificationType = notificationType,
                                    body = sourseUserName + ": " + Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    AdminUserID = adminUserID,
                                    NotificationType = notificationType,
                                    body = sourseUserName + ": " + Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendNotificationWhenSaveGroupChatStatus(string UserValue,
                                                                                                 string Messageoutput,
                                                                                                 string UserGCMId,
                                                                                                 int notificationType,
                                                                                                 string UserName,
                                                                                                 string GroupName,
                                                                                                 string GroupImage,
                                                                                                 string MemberNo,
                                                                                                 string CategoryCode,
                                                                                                 string GroupId,
                                                                                                 string SenderName,
                                                                                                 string SessionId,
                                                                                                 string GroupAdmin
            )
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (UserValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (UserValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (UserValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = UserGCMId,//"c4Qtv9MEVew:APA91bELe22JVkDxzpQ57c4cgvgdfr8Gl6wUIEtMEztbg_zYB4e3NXZJo1sP - o4cEqeH_GrptThQyaznhW4ZUr8mgaI2FblNvDtEldRdizlpl - vmxEpnVgQTkCzUxQI8cXLzN - sEafBA",
                                data = new
                                {
                                    UserName = UserName,
                                    GroupName = GroupName,
                                    GroupImage = GroupImage,
                                    MemberNo = MemberNo,
                                    CategoryCode = CategoryCode,
                                    GroupId = GroupId,
                                    SenderName = SenderName,
                                    SessionId = SessionId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = SenderName,
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (UserValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = UserGCMId,// "fv2iE4SPFEc:APA91bFqENOgCsKnEbtEPIM_UYVv02S8fYpP9CtjE-PFuUzXCtmnLCZNsKUgf3ib71RncB3ImvsPRrU3aSxEY4wLBPFZ9Ir-TqsaR2CPvF6KiwJ_qTE-OY3wltx7UA7rVqQ6-ugGbZit",
                                notification = new
                                {
                                    UserName = UserName,
                                    GroupName = GroupName,
                                    GroupImage = GroupImage,
                                    MemberNo = MemberNo,
                                    CategoryCode = CategoryCode,
                                    GroupId = GroupId,
                                    SenderName = SenderName,
                                    SessionId = SessionId,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = SenderName,
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (UserValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (UserValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }
        public static async Task<string> SendNotificationWhenHospitalMapHCPToPatientByAdmin(string userValue,
                                                                                                 string Messageoutput,
                                                                                                 string gcmId,
                                                                                                 int notificationType,
                                                                                                 string sessionid,
                                                                                                 string adminName,
                                                                                                 string chatBookAppID,
                                                                                                 string selectedHCPID,
                                                                                                 string consultationFee)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    ConsultationFee = consultationFee,
                                    MobileNumber = "",
                                    ChatBookAppID = chatBookAppID,
                                    SelectedHCPID = selectedHCPID,
                                    SessionId = sessionid,
                                    AdminName = adminName,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    ConsultationFee = consultationFee,
                                    MobileNumber = "",
                                    ChatBookAppID = chatBookAppID,
                                    SelectedHCPID = selectedHCPID,
                                    SessionId = sessionid,
                                    AdminName = adminName,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }

        public static async Task<string> SendNotificationWhenDiagnosticMapHCPToPatientByAdmin(string userValue,
                                                                                                string Messageoutput,
                                                                                                string gcmId,
                                                                                                int notificationType,
                                                                                                string sessionid,
                                                                                                string patientName,
                                                                                                string hospitalName,
                                                                                                string appointmentDate)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    MobileNumber = "",
                                    AppointmentDate = appointmentDate,
                                    SessionId = sessionid,
                                    hospitalName = hospitalName,
                                    NotificationType = notificationType,
                                    PatientName = patientName,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    MobileNumber = "",
                                    AppointmentDate = appointmentDate,
                                    hospitalName = hospitalName,
                                    SessionId = sessionid,
                                    NotificationType = notificationType,
                                    PatientName = patientName,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
                //Click2Clinic.Telangana.Repository.ExceptionLogger _exceptionLogger = null;
                //string jsonrequest = string.Empty;
                //jsonrequest = Newtonsoft.Json.JsonConvert.SerializeObject(new object[] { "UserValue: " + userValue, "Messageoutput: " + Messageoutput, "regIds: " + gcmId });
                //string ErrorSource = "Class Name: Helper.cs ;Method: SendNotification;InputRequest=" + jsonrequest;
                //_exceptionLogger = new Click2Clinic.Telangana.Repository.ExceptionLogger(new DBLogger());
                //await _exceptionLogger.LogException(ErrorSource, ex);
            }
            return sResponseFromServer;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            try
            {
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
            }
            catch (Exception ex)
            { }
            return dataTable;
        }

        public static async Task<string> SendNotificationToPatientWhenLeadAssgin(string userValue,
                                                                                string Messageoutput,
                                                                                string gcmId,
                                                                                int notificationType)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {

                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = "AAAAyr45QG4:APA91bFBLCtJOW - MJ - YxGFyoaJozMrCFXylwWSOtuE3tJDhTZg5vNSRRLq970jBoes_MI1_EaFKHKcH8mgKu - 0dw1_BT0UolWdao61XW9eZC_yao1nGP - PolLi8A1YIYdhWxscvDKW5K0_dRlsdcKv_x9660swoH6A";
                        //= ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        //      var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        WebRequest httpWebRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";
                        httpWebRequest.UseDefaultCredentials = true;
                        httpWebRequest.PreAuthenticate = true;
                        httpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {

                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }

                        var serializer = new JavaScriptSerializer();
                        json = serializer.Serialize(data);
                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAAyr45QG4:APA91bFBLCtJOW-MJ-YxGFyoaJozMrCFXylwWSOtuE3tJDhTZg5vNSRRLq970jBoes_MI1_EaFKHKcH8mgKu-0dw1_BT0UolWdao61XW9eZC_yao1nGP-PolLi8A1YIYdhWxscvDKW5K0_dRlsdcKv_x9660swoH6A"));
                        httpWebRequest.Headers.Add(string.Format("Sender: id={0}", "9177789922"));
                        httpWebRequest.ContentLength = byteArray.Length;
                        using (Stream dataStream = httpWebRequest.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            using (WebResponse tResponse = httpWebRequest.GetResponse())
                            {
                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {
                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {
                                        sResponseFromServer = tReader.ReadToEnd();
                                        string str = sResponseFromServer;
                                        //  strResponse = strResponse + str;
                                        //uow.UserRepository.updateNotificationStatus(responseNotification.NotificationId, true);
                                    }
                                }
                            }
                        }
                        //Stream dataStream = httpWebRequest.GetRequestStream();
                        //dataStream.Write(byteArray, 0, byteArray.Length);
                        //   dataStream.Close();
                        //    WebResponse tResponse = httpWebRequest.GetResponse();
                        //  dataStream = tResponse.GetResponseStream();
                        //    StreamReader tReader = new StreamReader(dataStream);
                        //   sResponseFromServer = tReader.ReadToEnd();
                        // tReader.Close();
                        // dataStream.Close();
                        // tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
            }
            return sResponseFromServer;
        }

        #region sample code
        public static string SendNotificationFromFirebaseCloud(string test)
        {
            var result = "-1";
            var webAddr = "https://fcm.googleapis.com/fcm/send";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "key=AAAAyr45QG4:APA91bFBLCtJOW-MJ-YxGFyoaJozMrCFXylwWSOtuE3tJDhTZg5vNSRRLq970jBoes_MI1_EaFKHKcH8mgKu-0dw1_BT0UolWdao61XW9eZC_yao1nGP-PolLi8A1YIYdhWxscvDKW5K0_dRlsdcKv_x9660swoH6A");
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //      ""to"": ""cILQkUuvQLypZ1ohT02e9q: APA91bGk - WLnuYc18H1vUfu3 - 1Def_lR70jWvjfGgd3T9RyF6aa1cfwq_DSsBPCNMjc1xfMDV2DMnvsLwuD2GV9FIZxqwGvCRooHR2tUBmfcBbpGO6kibILrG33pZLUQbdFgQ3ii - YdM"",                    
                string strNJson = @"{
                                    ""to"": cILQkUuvQLypZ1ohT02e9q:APA91bGk-WLnuYc18H1vUfu3-1Def_lR70jWvjfGgd3T9RyF6aa1cfwq_DSsBPCNMjc1xfMDV2DMnvsLwuD2GV9FIZxqwGvCRooHR2tUBmfcBbpGO6kibILrG33pZLUQbdFgQ3ii-YdM"",                    
                                    ""data"": {
                                        ""ShortDesc"": ""This is sample"",
                                        ""IncidentNo"": ""INC0010438"",
                                        ""Description"": ""This is sample""
                  },
                  ""notification"": {
                                ""title"": ""ServiceNow"",
                    ""text"": ""Hi Sai How are you!"",
                ""sound"":""default""
                  }
                        }";
                //string strnewjson =@"{""to"":"""+test+"\",}";
                //                string strNJson = @"{
                //""to"": ""test"",                    
                //                    ""data"": {
                //                        ""ShortDesc"": ""This is sample"",
                //                        ""IncidentNo"": ""INC0010438"",
                //                        ""Description"": ""This is sample""
                //  },
                //  ""notification"": {
                //                ""title"": ""ServiceNow"",
                //    ""text"": ""Hi Sai How are you!"",
                //""sound"":""default""
                //  }
                //        }";
                streamWriter.Write(strNJson);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }
        public static void Notifications()
        {
            string resend;
            do
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var objNotification = new
                {
                    // to = "fSkVbIDvRfyEsfb2iuxTwm:APA91bGZx1GdrIrpH-aJiN3z76zoU5XlHEKD2x-M178FPdFI_t_ADTsQHiCUJ8dah5DpEV8bCUTGUZq_DDQA4mpO70L14GVW_AGnRw3-iy9pXGDgaEvnw7DDkupyaP2CDBNgxKTRtMxI",
                    to ="",
                    data = new
                    {
                        title = "title",
                        body = "body",
                        icon = "/firebase-logo.png"
                    }
                };
                string jsonNotificationFormat = Newtonsoft.Json.JsonConvert.SerializeObject(objNotification);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsonNotificationFormat);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAAb-Xi2_4:APA91bELCbxdSHyXA3uF6dXlEIgajCyJC-jXeAgjEkW-0oGwfjTrF3Q9zwaODBf-9E1_bD5NKOY8l1lh6g0qAFMeYBHI1Ep2GPGksg5oQrLXQ0Gife6pzGbPd1hh__2Spft68-iOyShr"));
                                                                         //     AAAAb - Xi2_4:APA91bELCbxdSHyXA3uF6dXlEIgajCyJC - jXeAgjEkW - 0oGwfjTrF3Q9zwaODBf - 9E1_bD5NKOY8l1lh6g0qAFMeYBHI1Ep2GPGksg5oQrLXQ0Gife6pzGbPd1hh__2Spft68 - iOyShr
                tRequest.Headers.Add(string.Format("Sender: id={0}", "480598219774"));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();

                                FCMResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<FCMResponse>(responseFromFirebaseServer);
                                if (response.success == 1)
                                {
                              //      Console.WriteLine("succeeded");
                                }
                                else if (response.failure == 1)
                                {
                                 //   Console.WriteLine("failed");

                                }

                            }
                        }

                    }
                }
                resend = Console.ReadLine();
            } while (resend == "c");
        }
        public static async void NotifyAsync(string to, string title, string body)
        {
            try
            {
                // Get the server key from FCM console
                var serverKey = string.Format("key={0}", "AAAAb-Xi2_4:APA91bELCbxdSHyXA3uF6dXlEIgajCyJC-jXeAgjEkW-0oGwfjTrF3Q9zwaODBf-9E1_bD5NKOY8l1lh6g0qAFMeYBHI1Ep2GPGksg5oQrLXQ0Gife6pzGbPd1hh__2Spft68-iOyShr");

                // Get the sender id from FCM console
                var senderId = string.Format("id={0}", "480598219774");

                var data = new
                {
                    to,
                    notification = new { title, body }
                };

                // Using Newtonsoft.Json
                var jsonBody = JsonConvert.SerializeObject(data);

                using (var httpRequest = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send"))
                {
                    httpRequest.Headers.TryAddWithoutValidation("Authorization", serverKey);
                    httpRequest.Headers.TryAddWithoutValidation("Sender", senderId);
                    httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                    using (var httpClient = new HttpClient())
                    {
                        var result = await httpClient.SendAsync(httpRequest);

                        if (result.IsSuccessStatusCode)
                        {
                          //  return true;
                        }
                        else
                        {
                            // Use result.StatusCode to handle failure
                            // Your custom error handler here
                            //_logger.LogError($"Error sending notification. Status Code: {result.StatusCode}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               // _logger.LogError($"Exception thrown in Notify Service: {ex}");
            }

       //     return false;
        }
        #endregion

        public static async Task<string> SendInCaseOfSavePatientTransactionNotification(string userValue,
                                                                               string Messageoutput,
                                                                               string gcmId,
                                                                               int notificationType, string status, string PaymentTransactionCode, string Amount)
        {
            string sResponseFromServer = string.Empty;
            try
            {
                return await Task.Run(() =>
                {
                    #region  Ready Reference keys

                    string pushurl = string.Empty, SERVER_API_KEY = string.Empty;
                    if (userValue.ToLower().Contains("ios"))
                    {
                        pushurl = ConfigurationManager.AppSettings["IOSFCMUrl"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["IOSGCMAPIKey"];
                    }
                    else if (userValue.ToLower().Contains("android"))
                    {
                        pushurl = ConfigurationManager.AppSettings["AndroidGoogleAPI"];
                        SERVER_API_KEY = ConfigurationManager.AppSettings["AndriodAPIKey"];
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(pushurl) && !string.IsNullOrEmpty(SERVER_API_KEY))
                    {
                        object data = null;
                        object jsonData = null;
                        string json = null;

                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(pushurl);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Headers.Add("Authorization:key=" + SERVER_API_KEY);
                        httpWebRequest.Method = "POST";

                        if (userValue.ToLower().Contains("android"))
                        {
                            data = new
                            {
                                to = gcmId,
                                data = new
                                {
                                    PaymentTransactionCode = PaymentTransactionCode,
                                    Amount = Amount,
                                    Status = status,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default"
                                }
                            };

                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            jsonData = new
                            {
                                to = gcmId,
                                notification = new
                                {
                                    PaymentTransactionCode = PaymentTransactionCode,
                                    Amount = Amount,
                                    Status = status,
                                    NotificationType = notificationType,
                                    body = Messageoutput,
                                    title = ConfigurationManager.AppSettings["NotificationTitle"] != null ? ConfigurationManager.AppSettings["NotificationTitle"].ToString() : "Home Heal",
                                    icon = "home.png",
                                    sound = "default",
                                    //    content_available = true
                                },
                                priority = "high"
                            };
                        }

                        var serializer = new JavaScriptSerializer();
                        if (userValue.ToLower().Contains("android"))
                        {
                            json = serializer.Serialize(data);
                        }
                        else if (userValue.ToLower().Contains("ios"))
                        {
                            json = serializer.Serialize(jsonData);
                        }

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        httpWebRequest.ContentLength = byteArray.Length;
                        Stream dataStream = httpWebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse tResponse = httpWebRequest.GetResponse();
                        dataStream = tResponse.GetResponseStream();
                        StreamReader tReader = new StreamReader(dataStream);
                        sResponseFromServer = tReader.ReadToEnd();
                        tReader.Close();
                        dataStream.Close();
                        tResponse.Close();
                        return sResponseFromServer;
                    }
                    else
                    {
                        return sResponseFromServer;
                    }
                });
            }
            catch (Exception ex)
            {
            }
            return sResponseFromServer;
        }


    }
    public class RootObject
    {
        public Response Response { get; set; }
        public string ErrorMessage { get; set; }
        public int Status { get; set; }
    }
    public class Response
    {
        public string message_id { get; set; }
        public int message_count { get; set; }
        public double price { get; set; }
    }
    public class FCMResponse
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<FCMResult> results { get; set; }
    }
    public class FCMResult
    {
        public string message_id { get; set; }
    }
}
