﻿using System;
using Incus.Datalayer;
using System.Data.SqlClient;
using System.Threading;
using System.Data;
using System.Threading.Tasks;
using Incus.Models;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Collections.Generic;
using System.Net.Http;

namespace Incus.Datalayer
{
    public class IncusDal
    {
        IncusDBContext DL = new IncusDBContext();
        public async Task<DataSet> INCUS_VerifyOTPWeb(string UserID, string Password)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("INCUS_WebLoginVerify");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@phoneno", UserID);
                        cmd.Parameters.AddWithValue("@password", Password);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> SaveIncusUserRegistration(INCUSRegisterModel saveUserRegistration)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Incus_SaveRegistration]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Name", saveUserRegistration.Name);
                        cmd.Parameters.AddWithValue("@Phoneno", saveUserRegistration.Phoneno);
                        cmd.Parameters.AddWithValue("@Emailid", saveUserRegistration.Emailid);
                        cmd.Parameters.AddWithValue("@Password", saveUserRegistration.Password);
                        cmd.Parameters.AddWithValue("@City", saveUserRegistration.City);
                        cmd.Parameters.AddWithValue("@Registerfrom", saveUserRegistration.Registerfrom);
                        cmd.Parameters.AddWithValue("@GCMID", saveUserRegistration.GCMID);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> UpdatePhoneno(INCUSRegisterModel saveUserRegistration)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Incus_UpdatePhoneo]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Emailid", saveUserRegistration.Emailid);
                        cmd.Parameters.AddWithValue("@Phoneno", saveUserRegistration.Phoneno);
                        cmd.Parameters.AddWithValue("@City", saveUserRegistration.City);
                        cmd.Parameters.AddWithValue("@DOB", saveUserRegistration.DOB);
                        cmd.Parameters.AddWithValue("@GCMID", saveUserRegistration.GCMID);
                        cmd.Parameters.AddWithValue("@ProfilePic", saveUserRegistration.ProfilePic);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> GetExamCategoryDetails()
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetExamCategoryDetails]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetExamSubCategoryDetails(INCUSSubCategoryRequest req)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetExamSubCategoryDetailsNew]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        cmd.Parameters.AddWithValue("@CategoryID", req.CategoryID);
                        cmd.Parameters.AddWithValue("@Emailid", req.Emailid);
                        cmd.Parameters.AddWithValue("@Playtype", req.PlayType);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetTournamentSearchPin(GetTournamentSearchPin PinData)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Incus_GetTournamentSearchPin]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        cmd.Parameters.AddWithValue("@Phoneno", PinData.Phoneno);
                        cmd.Parameters.AddWithValue("@Pinnumber", PinData.Pinnumber);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> RegisterforGame(RegisterforGameInput verifyOtp)
        {
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_SaveGameRegistration", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Phoneno", verifyOtp.Phoneno);
                        sqlCmd.Parameters.AddWithValue("@CategoryID", verifyOtp.CategoryID);
                        sqlCmd.Parameters.AddWithValue("@SubCategoryID", verifyOtp.SubCategoryID);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> Incus_VerifyLogin(INCUSVerifyUserModel LoginID)
        {
            DataSet ds = new DataSet();
            //  AuctionEntity auctionEntityresponse = new AuctionEntity();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("Incus_VerifyLogin");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@MailID", LoginID.MailID);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> VerifyOTP(string strMobile, string strOTP, string deviceregid, string HospitalID, string UserValue)
        {
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand sqlCmd = new SqlCommand("Connect_VerifyOTPRequest", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@MobileNo", strMobile);
                        sqlCmd.Parameters.AddWithValue("@OTPValue", strOTP);
                        sqlCmd.Parameters.AddWithValue("@Deviceregid", deviceregid);
                        sqlCmd.Parameters.AddWithValue("@HospitalID", HospitalID);
                        sqlCmd.Parameters.AddWithValue("@UserValue", UserValue);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetQuizGameQuestions(AcceptChallengeReq acceptChallengeReq)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetQuizGameQuestions]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CategoryId", acceptChallengeReq.CategoryId);
                        cmd.Parameters.AddWithValue("@SubCategoryID", acceptChallengeReq.SubCategoryID);
                        cmd.Parameters.AddWithValue("@Phoneno", acceptChallengeReq.Phoneno);
                        sqlConn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> SaveAnswerForGame(AnswerforQuestion verifyOtp)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_saveGameAnswer", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Phoneno", verifyOtp.Phoneno);
                        sqlCmd.Parameters.AddWithValue("@QuestionCode", verifyOtp.QuestionCode);
                        sqlCmd.Parameters.AddWithValue("@SubCategoryid", verifyOtp.SubCategoryid);
                        sqlCmd.Parameters.AddWithValue("@CategoryID", verifyOtp.CategoryID);
                        sqlCmd.Parameters.AddWithValue("@UserAnswer", verifyOtp.UserAnswer);
                        sqlCmd.Parameters.AddWithValue("@AnswerStatus", verifyOtp.AnswerStatus);
                        sqlCmd.Parameters.AddWithValue("@TimeTaken", verifyOtp.TimeTaken);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> INCUS_GetBargraphContent(string QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetBargraphContent]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@QuestionCode", QuestionCode);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> INCUS_GetLeaderboardDashboard(LeaderboardRequest QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_LeaderBoardtest]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@QuestionCode", QuestionCode.QuestionCode);
                        cmd.Parameters.AddWithValue("@Phonenouserid", QuestionCode.Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> FinalResult(RegisterforGameInput QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Incus_FinalResult]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CategoryID", QuestionCode.CategoryID);
                        cmd.Parameters.AddWithValue("@SubCategoryID", QuestionCode.SubCategoryID);
                        cmd.Parameters.AddWithValue("@UserPhoneno", QuestionCode.Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> INCUS_PlayerHistory(string Phoneno)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_PlayerHistory]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Phoneno", Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #region Wallet Payment
        public async Task<DataSet> SavePaymentTransaction(PaymentModel verifyOtp)
        {
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_SavePaymentTransaction", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("Phoneno", verifyOtp.Phoneno);
                        sqlCmd.Parameters.AddWithValue("@PaymentTransactionID", verifyOtp.PaymentTransactionID);
                        sqlCmd.Parameters.AddWithValue("@BankTransactionID", verifyOtp.BankTransactionID);
                        sqlCmd.Parameters.AddWithValue("@BankResponseCode", verifyOtp.BankResponseCode);
                        sqlCmd.Parameters.AddWithValue("@PaymentStatus", verifyOtp.PaymentStatus);
                        sqlCmd.Parameters.AddWithValue("@PaymentType", verifyOtp.PaymentType);
                        sqlCmd.Parameters.AddWithValue("@Amount", verifyOtp.Amount);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> logout(LogoutModel Emailid)
        {
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand sqlCmd = new SqlCommand("INCUS_Logout", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("emailid", Emailid.emailid);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion
        public DataTable Connect_GetMaxLGNCode()
        {
            try
            {

                using (SqlConnection sqlConn = DL.MyGetConnection())
                {
                    SqlCommand sqlCmd = new SqlCommand("Connect_GetMaxLGNCode", sqlConn);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();
                    //sqlCmd.Parameters.AddWithValue("@countryname", countryname);
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    sqlConn.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public DataTable Connect_GetMaxCancelCode()
        {
            try
            {

                using (SqlConnection sqlConn = DL.MyGetConnection())
                {
                    SqlCommand sqlCmd = new SqlCommand("Connect_GetMaxCancelCode", sqlConn);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    sqlConn.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> GetReviewQuizGameQuestions(AcceptChallengeReq acceptChallengeReq)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetReviewQuizGameQuestions]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CategoryId", acceptChallengeReq.CategoryId);
                        cmd.Parameters.AddWithValue("@SubCategoryID", acceptChallengeReq.SubCategoryID);
                        cmd.Parameters.AddWithValue("@Phoneno", acceptChallengeReq.Phoneno);
                        sqlConn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> PaymentSubscriptionDetails(string Phoneno)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_PaymentSubscriptionDetails]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Phoneno", Phoneno);
                        sqlConn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> UserNotifications(string Phoneno)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetUserNotifications]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Phoneno", Phoneno);
                        sqlConn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> INCUSSampleNotification(string Phoneno)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Connect_samplenotification]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Phoneno", Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> Incus_VersionAPI()
        {
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_GetVersionAPI", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #region Reports on 10-10-2020
        public async Task<DataSet> Incus_ReportUsers()
        {
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_ReportUsers", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Bookmark
        public async Task<DataSet> SaveBookMark(BookmarkRequest bookmarkRequest)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_SaveBookmarks", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Phoneno", bookmarkRequest.Phoneno);
                        sqlCmd.Parameters.AddWithValue("@QuestionCode", bookmarkRequest.QuestionCode);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> Incus_GetBookmarks(string Phoneno)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_GetBookmarks", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Phoneno", Phoneno);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetQuestionBookmarkReview(BookmarkRequest QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetQuestionBookmarkReview]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@QuestionCode", QuestionCode.QuestionCode);
                        cmd.Parameters.AddWithValue("@Phoneno", QuestionCode.Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region Comments 
        public async Task<DataSet> SaveComments(CommentRequest commentRequest)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_SaveQuestionComments", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Phoneno", commentRequest.Phoneno);
                        sqlCmd.Parameters.AddWithValue("@QuestionCode", commentRequest.QuestionCode);
                        sqlCmd.Parameters.AddWithValue("@Comments", commentRequest.Comments);
                        sqlCmd.Parameters.AddWithValue("@ImageUrl", commentRequest.ImageUrl);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetQuestionComments(string QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Incus_GetQuestionComments]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@QuestionCode", QuestionCode);
                        sqlConn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> SaveReportComments(CommentReport commentRequest)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_SaveReportComment", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Phoneno", commentRequest.Phoneno);
                        sqlCmd.Parameters.AddWithValue("@QuestionCode", commentRequest.QuestionCode);
                        sqlCmd.Parameters.AddWithValue("@Commentid", commentRequest.Commentid);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> Incus_GetQuestionCommentsforReport()
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[Incus_GetQuestionCommentsforReport]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> Incus_RepublishComment(RepublishComment CommentID)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_RepublishComment", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@CommentID", CommentID.Commentid);
                        sqlCmd.Parameters.AddWithValue("@PublishStatus", CommentID.PublishStatus);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetQuestionDetails(BookmarkRequest QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetQuestionDetails]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@QuestionCode", QuestionCode.QuestionCode);
                        cmd.Parameters.AddWithValue("@Phoneno", QuestionCode.Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region WebQuestions
        public async Task<DataSet> INCUS_GetQuizQuestionsAll(QuestionSearchRequest Searchkey)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetQuizQuestionsAll]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FromCount", Searchkey.FromCount);
                        cmd.Parameters.AddWithValue("@ToCount", Searchkey.ToCount);
                        cmd.Parameters.AddWithValue("@Searchkey", Searchkey.SearchKey);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> UpdateSubCategoryName(UpdateTournamentName verifyOtp)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_UpdateTournamentName", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Pinnumber", verifyOtp.Pinnumber);
                        sqlCmd.Parameters.AddWithValue("@TournamentName", verifyOtp.TournamentName);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> UpdateQuestionbyRecord(QuestionforWeb verifyOtp)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("Incus_UpdateTournamentName", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@questioncode", verifyOtp.QuestionCode);
                        sqlCmd.Parameters.AddWithValue("@questionname", verifyOtp.QuestionText);
                        sqlCmd.Parameters.AddWithValue("@answer", verifyOtp.ActualAnswer);
                        sqlCmd.Parameters.AddWithValue("@Explanation", verifyOtp.ExplanationText);
                        sqlCmd.Parameters.AddWithValue("@explanation1", verifyOtp.ExplanationImage1);
                        sqlCmd.Parameters.AddWithValue("@explanation2", verifyOtp.ExplanationImage2);
                        sqlCmd.Parameters.AddWithValue("@explanation3", verifyOtp.ExplanationImage3);
                        sqlCmd.Parameters.AddWithValue("@explanation4", verifyOtp.ExplanationImage4);
                        sqlCmd.Parameters.AddWithValue("@explanation5", verifyOtp.ExplanationImage5);
                        sqlCmd.Parameters.AddWithValue("@explanation6", verifyOtp.ExplanationImage6);
                        sqlCmd.Parameters.AddWithValue("@explanation7", verifyOtp.ExplanationImage7);
                        sqlCmd.Parameters.AddWithValue("@explanation8", verifyOtp.ExplanationImage8);
                        sqlCmd.Parameters.AddWithValue("@explanation9", verifyOtp.ExplanationImage9);
                        sqlCmd.Parameters.AddWithValue("@explanation10", verifyOtp.ExplanationImage10);
                        sqlCmd.Parameters.AddWithValue("@topic", verifyOtp.Topic);
                        sqlCmd.Parameters.AddWithValue("@subject", verifyOtp.Subject);
                        sqlCmd.Parameters.AddWithValue("@OptionA", verifyOtp.OptionA);
                        sqlCmd.Parameters.AddWithValue("@OptionB", verifyOtp.OptionB);
                        sqlCmd.Parameters.AddWithValue("@OptionC", verifyOtp.OptionC);
                        sqlCmd.Parameters.AddWithValue("@OptionD", verifyOtp.OptionD);
                        //    sqlCmd.Parameters.AddWithValue("@OptionE", verifyOtp.OptionE);
                        sqlCmd.Parameters.AddWithValue("@questionimageurl", verifyOtp.QuestionIMAGEURL);
                        sqlCmd.Parameters.AddWithValue("@questionvideourl", verifyOtp.QuestionVideoURL);
                        sqlCmd.Parameters.AddWithValue("@questionaudiourl", verifyOtp.QuestionAudioURL);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> GetCorrectwrongCounts(GetCorrectwrongCountsRequest QuestionCode)
        {
            DataSet ds = new DataSet();
            try
            {
                return await Task.Run(() =>
                {
                    using (SqlConnection sqlConn = DL.MyGetConnection())
                    {
                        SqlCommand cmd = new SqlCommand("[INCUS_GetCorrectWrongAnswerCount]");
                        cmd.Connection = sqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AnswerStatus", QuestionCode.AnswerStatus);
                        cmd.Parameters.AddWithValue("@Phoneno", QuestionCode.Phoneno);
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        #endregion

        #region 7th nov 2020
        public async Task<DataSet> Incus_SaveCourses(Courses verifyOtp)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("[Incus_SaveCourses]", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@CourseName", verifyOtp.CourseName);
                        sqlCmd.Parameters.AddWithValue("@CourseDescription", verifyOtp.CourseDescription);
                        sqlCmd.Parameters.AddWithValue("@AboutAuthor", verifyOtp.AboutAuthor);
                        sqlCmd.Parameters.AddWithValue("@CategoryID", verifyOtp.CategoryID);
                        sqlCmd.Parameters.AddWithValue("@TotalMCQs", verifyOtp.TotalMCQs);
                        sqlCmd.Parameters.AddWithValue("@TotalTournaments", verifyOtp.TotalTournaments);
                        sqlCmd.Parameters.AddWithValue("@Price", verifyOtp.Price);
                        sqlCmd.Parameters.AddWithValue("@SubscriptionDuration", verifyOtp.SubscriptionDuration);
                        sqlCmd.Parameters.AddWithValue("@CourseImageURL", verifyOtp.CourseImageURL);
                        sqlCmd.Parameters.AddWithValue("@CreatedBy", verifyOtp.CreatedBy);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> Incus_GetCourses(string UserID)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("[Incus_GetCoursesDetails]", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@CourseName", UserID);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }

        }
        public async Task<DataSet> INCUS_SaveSubCategory(INCUSSubcategoryPL saveHCPRegistration)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("[INCUS_SaveSubcategoryAngular]", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@CategoryID", saveHCPRegistration.CategoryID);
                        sqlCmd.Parameters.AddWithValue("@CourseID", saveHCPRegistration.CourseID);
                        sqlCmd.Parameters.AddWithValue("@SubCategoryName", saveHCPRegistration.SubCategoryName);
                        sqlCmd.Parameters.AddWithValue("@Testdetails", saveHCPRegistration.Description);
                        sqlCmd.Parameters.AddWithValue("@SponsoredBy", saveHCPRegistration.SponsoredBy);
                        sqlCmd.Parameters.AddWithValue("@PrizeValue", saveHCPRegistration.PrizeOffered);
                        sqlCmd.Parameters.AddWithValue("@Notification", saveHCPRegistration.Notification);
                        sqlCmd.Parameters.AddWithValue("@TypeofTournament", saveHCPRegistration.TypeofTournament);
                        sqlCmd.Parameters.AddWithValue("@TeacherDrivenType", saveHCPRegistration.TeacherDrivenType);
                        sqlCmd.Parameters.AddWithValue("@dateoftournament", saveHCPRegistration.DateofGame);
                        sqlCmd.Parameters.AddWithValue("@TimeSlots", saveHCPRegistration.TimeSlots);
                        sqlCmd.Parameters.AddWithValue("@Timezone", saveHCPRegistration.Timezone);
                        sqlCmd.Parameters.AddWithValue("@SubCategoryImageURL", saveHCPRegistration.SubCategoryImageURL);
                        sqlCmd.Parameters.AddWithValue("@TournamentStatus", "ACTIVE");
                        sqlCmd.Parameters.AddWithValue("@Createdby", saveHCPRegistration.Createdby);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<DataSet> INCUS_SaveQuestion(SaveQuestionPL saveHCPRegistration)
        {
            SqlConnection sqlConn = DL.MyGetConnection();
            try
            {
                return await Task.Run(() =>
                {
                    DataSet ds = new DataSet();
                    using (sqlConn)
                    {
                        SqlCommand sqlCmd = new SqlCommand("[INCUS_SaveQuestionDetails]", sqlConn);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlConn.Open();
                        sqlCmd.Parameters.AddWithValue("@Questionname", saveHCPRegistration.Questionname);
                        sqlCmd.Parameters.AddWithValue("@Answer", saveHCPRegistration.Answer);
                        sqlCmd.Parameters.AddWithValue("@Createdby", saveHCPRegistration.Createdby);
                        sqlCmd.Parameters.AddWithValue("@Subcategoryid", saveHCPRegistration.Subcategoryid);
                        sqlCmd.Parameters.AddWithValue("@Categoryid", saveHCPRegistration.Categoryid);
                        sqlCmd.Parameters.AddWithValue("@Explanation", saveHCPRegistration.Explanation);
                        sqlCmd.Parameters.AddWithValue("@Questionimageurl", saveHCPRegistration.Questionimageurl);
                        sqlCmd.Parameters.AddWithValue("@Explanation1", saveHCPRegistration.Explanation1);
                        sqlCmd.Parameters.AddWithValue("@Explanation2", saveHCPRegistration.Explanation2);
                        sqlCmd.Parameters.AddWithValue("@Explanation3", saveHCPRegistration.Explanation3);
                        sqlCmd.Parameters.AddWithValue("@Explanation4", saveHCPRegistration.Explanation4);
                        sqlCmd.Parameters.AddWithValue("@Explanation5", saveHCPRegistration.Explanation5);
                        sqlCmd.Parameters.AddWithValue("@Explanation6", saveHCPRegistration.Explanation6);
                        sqlCmd.Parameters.AddWithValue("@Explanation7", saveHCPRegistration.Explanation7);
                        sqlCmd.Parameters.AddWithValue("@Explanation8", saveHCPRegistration.Explanation8);
                        sqlCmd.Parameters.AddWithValue("@Explanation9", saveHCPRegistration.Explanation9);
                        sqlCmd.Parameters.AddWithValue("@Explanation10", saveHCPRegistration.Explanation10);
                        sqlCmd.Parameters.AddWithValue("@Topic", saveHCPRegistration.Topic);
                        sqlCmd.Parameters.AddWithValue("@Subject", saveHCPRegistration.Subject);
                        sqlCmd.Parameters.AddWithValue("@OptionA", saveHCPRegistration.OptionA);
                        sqlCmd.Parameters.AddWithValue("@OptionB", saveHCPRegistration.OptionB);
                        sqlCmd.Parameters.AddWithValue("@OptionC", saveHCPRegistration.OptionC);
                        sqlCmd.Parameters.AddWithValue("@OptionD", saveHCPRegistration.OptionD);
                        sqlCmd.Parameters.AddWithValue("@OptionE", saveHCPRegistration.OptionE);
                        sqlCmd.Parameters.AddWithValue("@QuestionVideoURL", saveHCPRegistration.QuestionVideoURL);
                        sqlCmd.Parameters.AddWithValue("@QuestionAudioURL", saveHCPRegistration.QuestionAudioURL);
                        sqlCmd.Parameters.AddWithValue("@Questiontime", saveHCPRegistration.Questiontime);
                        sqlCmd.Parameters.AddWithValue("@Courseid", saveHCPRegistration.Courseid);
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        da.Fill(ds);

                        sqlConn.Close();
                    }
                    return ds;
                });
            }
            catch (Exception ex)
            {
                sqlConn.Close();
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion
    }
}
