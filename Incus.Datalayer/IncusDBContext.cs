﻿using Incus.Core.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incus.Datalayer
{
    public class IncusDBContext
        {
            #region Declaration of ADO.NET Objects
            private static SqlConnection con;
            private static SqlCommand cmd;
            private static SqlDataReader Dr;
            private static SqlDataAdapter Da;
            private static DataSet DS;
            private static DataTable DT;

            #endregion
            public SqlConnection MyGetConnection()
            {
                var DeploymentType = CoreHelper.GetDeploymentType();
                if (DeploymentType.ToLower().Contains("live") || DeploymentType.ToLower().Contains("prod"))
                {
                    //if (CoreHelper.IsDBEncriptAndDecriptRequired().ToLower() == "true")
                    //    return (new SqlConnection(CoreHelper.Decrypt(ConfigurationManager.ConnectionStrings["DoctorApp_LiveOrProdWithEncript"].ConnectionString)));
                    //else
                        return (new SqlConnection(ConfigurationManager.ConnectionStrings["DoctorApp_LiveOrProdNotEncript"].ConnectionString));
                }
                else
                {
                    if (CoreHelper.IsDBEncriptAndDecriptRequired().ToLower() == "true")
                        return (new SqlConnection(CoreHelper.Decrypt(ConfigurationManager.ConnectionStrings["DoctorApp_DevWithEncript"].ConnectionString)));
                    else
                        return (new SqlConnection(ConfigurationManager.ConnectionStrings["DoctorApp_DevNotEncript"].ConnectionString));
                }
            }
            public SqlConnection MyQuestionGetConnection()
            {
                var DeploymentType = CoreHelper.GetDeploymentType();
                if (DeploymentType.ToLower().Contains("live") || DeploymentType.ToLower().Contains("prod"))
                {
                    if (CoreHelper.IsDBEncriptAndDecriptRequired().ToLower() == "true")
                        return (new SqlConnection(CoreHelper.Decrypt(ConfigurationManager.ConnectionStrings["Umedico_DevNotEncript"].ConnectionString)));
                    else
                        return (new SqlConnection(ConfigurationManager.ConnectionStrings["Umedico_DevNotEncript"].ConnectionString));
                }
                else
                {
                    if (CoreHelper.IsDBEncriptAndDecriptRequired().ToLower() == "true")
                        return (new SqlConnection(CoreHelper.Decrypt(ConfigurationManager.ConnectionStrings["Umedico_DevNotEncript"].ConnectionString)));
                    else
                        return (new SqlConnection(ConfigurationManager.ConnectionStrings["Umedico_DevNotEncript"].ConnectionString));
                }
            }
            public SqlConnection MyGetConnectionForLead()
            {
                return (new SqlConnection(ConfigurationManager.ConnectionStrings["Hims_HIMSLEadDb"].ConnectionString));
            }

            public SqlConnection MyChatDataConnection()
            {
                if (CoreHelper.IsDBEncriptAndDecriptRequired().ToLower() == "true")
                    return (new SqlConnection(CoreHelper.Decrypt(ConfigurationManager.ConnectionStrings["chatConStr"].ConnectionString)));
                else
                    return (new SqlConnection(ConfigurationManager.ConnectionStrings["chatConStrNotEncript"].ConnectionString));
            }

            #region MyRecordExistCheck(string TempQuery)
            public bool MyRecordExistCheck(string CommandString)
            {

                bool key = false;
                SqlConnection SqlCon = MyGetConnection();
                SqlCommand com = new SqlCommand(CommandString, SqlCon);
                com.Connection.Open();
                SqlDataReader dr = com.ExecuteReader();
                if (dr.HasRows)
                {
                    key = true;
                    dr.Close();
                }
                com.Dispose();
                SqlCon.Close();

                return key;
            }
            #endregion
            public DataTable MyExecuteQueryDataTable(string SqlQuery)
            {
                DT = new DataTable();
                con = MyGetConnection();
                using (con)
                {
                    Da = new SqlDataAdapter(SqlQuery, con);
                    Da.Fill(DT);
                    Da.Dispose();
                }
                return (DT);
            }
            public DataSet MyExecuteQuery(string SqlQuery)
            {
                DS = new DataSet();
                con = MyGetConnection();
                using (con)
                {
                    Da = new SqlDataAdapter(SqlQuery, con);
                    Da.Fill(DS);
                    Da.Dispose();
                }
                return (DS);
            }
            public string MyExecuteScalar(string SqlQuery)
            {
                string ret = "";
                con = MyGetConnection();
                con.Open();
                using (con)
                {
                    cmd = new SqlCommand(SqlQuery, con);
                    Dr = cmd.ExecuteReader();
                    if (Dr.Read())
                    {
                        ret = Dr[0].ToString();
                    }
                }
                return (ret);
            }
            public string MyExecuteScalar1(string SqlQuery)
            {
                string ret = "";
                con = MyGetConnection();
                con.Open();
                using (con)
                {
                    cmd = new SqlCommand(SqlQuery, con);
                    Dr = cmd.ExecuteReader();
                    if (Dr.Read())
                    {
                        Dr.Close();
                        ret = Dr[0].ToString();

                    }

                }
                return (ret);

            }
            #region Method for GetData from Database put into DataReader Object

            public SqlDataReader GetDataBaseRecords(string CommandString)
            {
                con = MyGetConnection();
                con.Open();
                try
                {
                    cmd = new SqlCommand(CommandString, con);
                    Dr = cmd.ExecuteReader();


                }
                catch (SqlException ex)
                {
                    throw new ArgumentException(ex.Message);
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(ex.Message);
                }
                finally
                {
                    if (con != null)
                        if (con.State != ConnectionState.Open)
                            con.Close();

                    if (cmd != null)
                        cmd.Dispose();


                }

                return Dr;
            }


            #endregion
            public int MyExecuteNonQuery(string SqlQuery)
            {
                int ret = -1;
                con = MyGetConnection();
                con.Open();
                using (con)
                {
                    cmd = new SqlCommand(SqlQuery, con);
                    ret = cmd.ExecuteNonQuery();
                }
                return (ret);
            }
            public int MyExecuteNonQuery(string CheckSql, string SqlQuery)
            {
                int ret = -1;
                con = MyGetConnection();
                con.Open();
                using (con)
                {
                    cmd = new SqlCommand(CheckSql, con);
                    Dr = cmd.ExecuteReader();
                    if (Dr.HasRows)
                    {
                        Dr.Close();
                    }
                    else
                    {
                        Dr.Close();
                        cmd.Dispose();
                        cmd.CommandText = SqlQuery;
                        ret = cmd.ExecuteNonQuery();
                    }
                }
                return (ret);
            }
            public int MyExecuteNonQuery1(string CheckSql, string SqlQuery)
            {
                int ret = -1;
                con = MyGetConnection();
                con.Open();
                using (con)
                {
                    cmd = new SqlCommand(CheckSql, con);
                    Dr = cmd.ExecuteReader();
                    if (Dr.HasRows)
                    {
                        Dr.Close();
                        cmd.Dispose();
                        cmd.CommandText = SqlQuery;
                        ret = cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        Dr.Close();
                    }
                }
                return (ret);
            }

        }
    }
