﻿using Incus.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incus.Datalayer
{
    public class ExceptionDal
    {
        IncusDBContext DL = new IncusDBContext();
        public async Task<bool> LogMessage(ExceptionLogEntity exlogentity)
        {
            return await Task.Run(() =>
            {
                using (SqlConnection sqlConn = DL.MyGetConnection())
                {
                        //SqlCommand cmd = new SqlCommand("C2C_uspInsertsAuctionLogs");

                        SqlCommand cmd = new SqlCommand("Incus_uspInsertsAuctionLogsnew");
                    cmd.Connection = sqlConn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AppName", exlogentity.appName);
                    cmd.Parameters.AddWithValue("@Request", exlogentity.requestObj);
                    cmd.Parameters.AddWithValue("@ErrorMessage", exlogentity.errorMsg);
                    cmd.Parameters.AddWithValue("@InnerMessage", exlogentity.innerMessage);
                    cmd.Parameters.AddWithValue("@StackTrace", exlogentity.stackTrace);
                    sqlConn.Open();
                    cmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
                return true;
            });
        }
    }
}
